from django.db import models

# Create your models here.

class Collection(models.Model):
    NOM_COLLECTION =(
            ('S1','Sentinel-1'),
            ('S2','Sentinel-2'),
            ('MV','SoilMoisture'),
            ('NDVI','NDVI'),
            ('SEG','Segmentation'),
            ('DEM','Digital Elevation Model')
        )
    nom_collection = models.CharField(max_length=10, choices=NOM_COLLECTION)
    
    
    def __str__(self):
        return self.nom_contributeur

class Dataset(models.Model):
    NOM_SATELLITE =(
            ('S1A','Sentinel-1A'),
            ('S1B','Sentinel-1B'),
            ('S2A','Sentinel-2A'),
            ('S2B','Sentinel-2B'),
            ('SRTM','SRTM')
        )
    
    nom_dataset = models.CharField(max_length=500)
    
    collection = models.ForeignKey(Collection, on_delete=models.CASCADE, null=True)
    
    #calculer a partir du nom
    instrument = models.CharField(max_length=100)
    
    sensing_date = models.DateField()
    sensing_time = models.TimeField()
    
    footprint = models.FilePathField()
    
    dirpath =models.FilePathField()
    