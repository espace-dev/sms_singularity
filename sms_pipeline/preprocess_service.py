#!/usr/bin/python
'''
Created on 13 nov. 2019

@author: loic
'''
import os,datetime, math, subprocess, glob
import argparse
import otbApplication
from osgeo import gdal, osr, ogr
from subprocess import Popen, PIPE
from configparser import ConfigParser



class exmosaic_pipeline():
    
    def read_config(self, path:str=None):
        config = ConfigParser()
        config.read(path)
    
        sections_dict = {}
    
        # get all defaults
        defaults = config.defaults()
        temp_dict = {}
        for key in defaults.keys():
            temp_dict[key] = defaults[key]
    
        sections_dict['default'] = temp_dict
    
        # get sections and iterate over each
        sections = config.sections()
        
        for section in sections:
            options = config.options(section)
            temp_dict = {}
            for option in options:
                temp_dict[option] = config.get(section,option)
            
            sections_dict[section] = temp_dict
    
        return sections_dict
    
    def __init__(self, args):
        self.args = args
        self.globconfig = {
            "urlsrmt":"http://step.esa.int/auxdata/dem/SRTMGL1/",
            "srtmhgtzip":"/data/SRTM/SRTMHGTZIP",
            "demtif":"/data/SRTM/SRTMHGTZIP/TIF",
            "geoid":"/work/python/data/egm96.grd"
            }
        if os.path.exists("/work/python/config.ini"):
            self.globconfig = self.read_config("/work/python/config.ini")["DEMDATA"]
        if not os.path.exists(self.globconfig["srtmhgtzip"]):
            print("Can't find SRTM folder. Creating it at :"+self.globconfig["srtmhgtzip"])
            os.makedirs(self.globconfig["srtmhgtzip"])
    
    
    def process(self, args):
        
        def search_files(directory='.', resolution='NDVI', extension='tif', fictype='f'):
            images=[]
            extension = extension.lower()
            resolution = resolution.lower()
            for dirpath, dirnames, files in os.walk(directory):
                if fictype == 'f':
                    for name in files:
            #            print(os.path.join(dirpath, name) + " test")
                        if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 :
                            
            #                print(os.path.join(dirpath, name) + " OK")
                            abspath = os.path.abspath(os.path.join(dirpath, name))
                            images.append(abspath)
                elif fictype == 'd':
                    for dirname in dirnames:
            #            print(os.path.join(dirpath, name) + " test")
                        if extension and dirname.lower().endswith(extension) and dirname.lower().find(resolution) >= 0 :
                            
            #                print(os.path.join(dirpath, name) + " OK")
                            abspath = os.path.abspath(os.path.join(dirpath, dirname))
                            images.append(abspath)
                else:
                    print("search_files type error")
                    exit
                    
            return images
        
        def process_command(cmd):
            print("Starting : "+" ".join(cmd))
            p = Popen(cmd, stdout=PIPE)
        #    p.wait()
            output = p.communicate()[0]
            if p.returncode != 0: 
                print("process failed %d : %s" % (p.returncode, output))
            print("#################################################")
            return p.returncode
        
        def get_img_extend(img):
            raster = gdal.Open(img)
            proj = osr.SpatialReference(wkt=raster.GetProjection())
            
            upx, xres, xskew, upy, yskew, yres = raster.GetGeoTransform()
            cols = raster.RasterXSize
            rows = raster.RasterYSize
             
            ulx = upx + 0*xres + 0*xskew
            uly = upy + 0*yskew + 0*yres
             
            llx = upx + 0*xres + rows*xskew
            lly = upy + 0*yskew + rows*yres
             
            lrx = upx + cols*xres + rows*xskew
            lry = upy + cols*yskew + rows*yres
             
            urx = upx + cols*xres + 0*xskew
            ury = upy + cols*yskew + 0*yres
            
            pointLL, pointUL, pointUR, pointLR = ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint)
            pointLL.AddPoint(llx, lly)
            pointUR.AddPoint(urx, ury)
            pointUL.AddPoint(ulx, uly)
            pointLR.AddPoint(lrx, lry)
            
            if not proj.IsGeographic() :
                
                outSpatialRef = osr.SpatialReference()
                outSpatialRef.ImportFromEPSG(4326)
                coordTransform = osr.CoordinateTransformation(proj, outSpatialRef)
                
                pointLL.Transform(coordTransform)
                pointUR.Transform(coordTransform)
                pointUL.Transform(coordTransform)
                pointLR.Transform(coordTransform)
                
            return pointLL, pointUL, pointUR, pointLR, cols, rows


        def get_img_intersection(img,ref):
    
            pointLL, pointUL, pointUR, pointLR, cols, rows = get_img_extend(img)
            ring = ogr.Geometry(ogr.wkbLinearRing)
            ring.AddPoint(pointLL.GetX(), pointLL.GetY())
            ring.AddPoint(pointUL.GetX(), pointUL.GetY())
            ring.AddPoint(pointUR.GetX(), pointUR.GetY())
            ring.AddPoint(pointLR.GetX(), pointLR.GetY())
            ring.AddPoint(pointLL.GetX(), pointLL.GetY())
            
            # Create polygon
            imgpoly = ogr.Geometry(ogr.wkbPolygon)
            imgpoly.AddGeometry(ring)
            
            
            pointLL, pointUL, pointUR, pointLR, cols, rows = get_img_extend(ref)
            ring = ogr.Geometry(ogr.wkbLinearRing)
            ring.AddPoint(pointLL.GetX(), pointLL.GetY())
            ring.AddPoint(pointUL.GetX(), pointUL.GetY())
            ring.AddPoint(pointUR.GetX(), pointUR.GetY())
            ring.AddPoint(pointLR.GetX(), pointLR.GetY())
            ring.AddPoint(pointLL.GetX(), pointLL.GetY())
            
            # Create polygon
            refpoly = ogr.Geometry(ogr.wkbPolygon)
            refpoly.AddGeometry(ring)
            
            if refpoly.Within(imgpoly):
                return refpoly
            
            return refpoly.Intersection(imgpoly)
        
        def find_srtm_hgt_name(llx, lly , urx, ury ):
    
            pointLL, pointUR = ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint)
            pointLL.AddPoint(llx, lly)
            pointUR.AddPoint(urx, ury)
            
            tilesname=[]
            print("LowerLeft="+pointLL.ExportToWkt()+";UpperRight="+pointUR.ExportToWkt())
            
            for o in range(math.ceil(pointUR.GetX()) - math.floor(pointLL.GetX())):
                for a in range(math.ceil(pointUR.GetY()) - math.floor(pointLL.GetY())):
                    lat =  math.floor(pointLL.GetY()) + a
                    lon =  math.floor(pointLL.GetX()) + o
                    if lat >= 0 :
                        hem = 'N'
                    else : 
                        hem = 'S'
                        lat = abs(lat)
                    if lon >= 0 :
                        grw = 'E'
                    else : 
                        grw = 'W'
                        lon = abs(lon)
                    tilesname.append(hem+f'{lat:02}'+grw+f'{lon:003}')
                    
            return tilesname
        
        def prepare_srtm_hgt(tilesname):    
            print("Checking SRTM tiles: "+str(tilesname))
            tilefiles = []
            if len(tilesname) == 0:
                print("bug")
                exit()
            
            srtmsuf = ".SRTMGL1.hgt.zip"   
            
            tiftiles=[]
            for tile in tilesname :
                tilef = os.path.join(self.globconfig["srtmhgtzip"],tile + srtmsuf)
                if not os.path.exists(tilef):
                    print("Download SRTM tile : "+tilef)
                    cmd=["wget","-P", self.globconfig['srtmhgtzip'],self.globconfig["urlsrmt"]+tile + srtmsuf]
                    rcode = process_command(cmd)
                    
                    if rcode != 0:
                        continue
                
                hgtdir = os.path.join(self.globconfig["srtmhgtzip"],"HGT")
                tifdir = os.path.join(self.globconfig["srtmhgtzip"],"TIF")
                if not os.path.exists(hgtdir):
                    os.mkdir(hgtdir)
                if not os.path.exists(tifdir):
                    os.mkdir(tifdir)
                    
                tilehgt = os.path.join(hgtdir,tile+".hgt")
                tiletif = os.path.join(tifdir,tile+".tif")
                if not os.path.exists(tilehgt):
                    print("Unzip SRTM tile : "+tilehgt)
                    cmd=['unzip','-d',hgtdir,tilef]   
                    process_command(cmd)
                
                if not os.path.exists(tiletif):
                    print("Convert SRTM tile : "+tiletif)
                    cmd=['gdal_translate',tilehgt,tiletif]   
                    process_command(cmd)
                
                tiftiles.append(tiletif)
            
            print("Ok for SRTM tiles: "+str(tilesname))
            return tiftiles

        def create_zone_dem_and_slope(ref,zone,outdir):
    
            outdem = os.path.join(outdir,"SRTM_"+zone+".TIF")
            if os.path.exists(outdem) and not args.overwrite :
                return outdem
                
            #get tiles
            pointLL, pointUL, pointUR, pointLR, cols, rows = get_img_extend(ref)
            
            tilesname = find_srtm_hgt_name(pointLL.GetX()-1.5, pointLL.GetY()-1 , pointUR.GetX()+1.5, pointUR.GetY()+1 )
            tilefiles = prepare_srtm_hgt(tilesname)
            
            if len(tilefiles) == 0 :
                print("Error : Can't find tiles")
                exit()
            
            print("Generating DEM file on ref:"+ref)
            
            mosapp = otbApplication.Registry.CreateApplication("Mosaic")
            mosparams = {"il":tilefiles, "out":"mosatemp.tif"}
            mosapp.SetParameters(mosparams)
            mosapp.Execute()
            
            supapp = otbApplication.Registry.CreateApplication("Superimpose")
            supapp.SetParameterString("inr", ref)
            supapp.SetParameterInputImage("inm", mosapp.GetParameterOutputImage("out"))
            supapp.SetParameterString("out", outdem)
            supapp.ExecuteAndWriteOutput()
            
            #Compute slope & aspect
            print("Generating slope file...")
            outslope = os.path.join(outdir,"SLOPE_"+zone+".TIF")
            cmd=['gdaldem', 'slope', '-p', outdem, outslope]
            
            process_command(cmd)
            
            return outdem
        
            
        def create_tmp_shp(name, poly, outdir):

            shpfile = name+".shp"
            outshp = os.path.join(outdir,shpfile)
            
            driver = ogr.GetDriverByName("ESRI Shapefile")
                
            # create the data source
            data_source = driver.CreateDataSource(outshp)
            
            # create the spatial reference, WGS84
            srs = osr.SpatialReference()
            srs.ImportFromEPSG(4326)
            
            # create the layer
            layer = data_source.CreateLayer(name, srs, ogr.wkbMultiPolygon)
             
            # create the feature
            feature = ogr.Feature(layer.GetLayerDefn())
            
            feature.SetGeometry(poly)
            # Create the feature in the layer (shapefile)
            layer.CreateFeature(feature)
            
            return outshp

        def exmos_pipeline(sarfiles, ref, outfile):
            
            print("processing.......................................................")

            outdir = os.path.dirname(outfile)
            appdict={"ExtractShp":[],"ExtractROI":[],
                     "Mosaic":None,"BandMath":None,"ManageNoData":None
                     }
            
            d=0
            delsar = []
            for sar in sarfiles:
                interpoly = get_img_intersection(sar,ref)
                print(interpoly)
                if None == interpoly or interpoly.IsEmpty():
                    print("Not using "+sar)
                    delsar.append(d)
                d+=1
            for dd in delsar:
                sarfiles.pop(dd)
                
            if len(sarfiles) == 0:
                print("ERROR: Sar files don't intersect reference.")
                print("Passing....")
                return 1
            i=0
            for sar in sarfiles:
                
                interpoly = get_img_intersection(sar,ref)
        #         shpname = os.path.basename(sar)[:-5]
                appdict["ExtractShp"].append(create_tmp_shp("tmppoly"+str(i), interpoly, outdir)) #shpname

                appdict["ExtractROI"].append(otbApplication.Registry.CreateApplication("ExtractROI"))
                appdict["ExtractROI"][i].SetParameterString("in",sar)
                appdict["ExtractROI"][i].SetParameterString("out",str(i)+"ExtractROI.tif") 
                appdict["ExtractROI"][i].SetParameterString("mode","fit") 
                appdict["ExtractROI"][i].SetParameterString("mode.fit.vect",appdict["ExtractShp"][i]) 
                appdict["ExtractROI"][i].Execute()
                
                i+=1
                
            if len(sarfiles) > 1:
                    
                appdict["Mosaic"] = otbApplication.Registry.CreateApplication("Mosaic")
                for j in range(len(sarfiles)):
                    appdict["Mosaic"].AddImageToParameterInputImageList("il", appdict["ExtractROI"][j].GetParameterOutputImage("out"))
                appdict["Mosaic"].SetParameterString("out",  "mosatemp.tif")
                appdict["Mosaic"].Execute()
                
                appdict["BandMath"] = otbApplication.Registry.CreateApplication("BandMath")
                appdict["BandMath"].AddImageToParameterInputImageList("il", appdict["Mosaic"].GetParameterOutputImage("out"))
                appdict["BandMath"].SetParameterString("out", "BMtemp.tif")
                appdict["BandMath"].SetParameterString("exp", "im1b1<=0?0:im1b1")
                appdict["BandMath"].Execute()
                
            else : 
                
                appdict["BandMath"] = otbApplication.Registry.CreateApplication("BandMath")
                appdict["BandMath"].AddImageToParameterInputImageList("il", appdict["ExtractROI"][0].GetParameterOutputImage("out"))
                appdict["BandMath"].SetParameterString("out", "BMtemp.tif")
                appdict["BandMath"].SetParameterString("exp", "im1b1<=0?0:im1b1")
                appdict["BandMath"].Execute()
                
            
            appdict["ManageNoData"] = otbApplication.Registry.CreateApplication("ManageNoData")
            appdict["ManageNoData"].SetParameterInputImage("in", appdict["BandMath"].GetParameterOutputImage("out"))
            appdict["ManageNoData"].SetParameterString("out", outfile+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")
            appdict["ManageNoData"].SetParameterString("mode", "changevalue")
            appdict["ManageNoData"].ExecuteAndWriteOutput()
            
            
            shapefiles = glob.glob(appdict["ExtractShp"][0][:-5]+"*")
            for s in shapefiles:
                os.remove(s)
            
            print("done.............................................................")
            
            return 0
            
        
        ##################################"
        #     MAIN
        ##################################
        
        if not os.path.exists(args.outdir):
            os.mkdir(args.outdir)
        
        procstart = datetime.datetime.now()
        
        if args.dem :
            demdir = os.path.join(args.outdir,"DEM")
            if not os.path.exists(demdir):
                os.mkdir(demdir)
            zonedemfile = create_zone_dem_and_slope(args.inref,args.zone,demdir)
        
        indir=[]
        indir=search_files(args.indir, 'S1', 'data', 'd')
        
        dblpolar = False
        if args.polar == "vvvh":
            dblpolar = True
            args.polar = "vv"
            
        
        dejafait=[]
        for filed in indir:
            if filed in dejafait:
                continue
            
            bname = os.path.basename(filed).split(".")[0]
            splitbname = bname.split("_")
            plateforme = splitbname[0]
            acqdate_start = splitbname[4]  
            acqdate_end = splitbname[5]
            absorbit = splitbname[6]
            
            outfilebase = os.path.join(args.outdir,"_".join(splitbname[:3])+"_"+args.zone.upper()+"_"+acqdate_start)
            outfile = outfilebase +"_"+args.polar.upper() +".TIF"
            if dblpolar:
                outfile2 = outfilebase +"_VH.TIF"
            
    #         file = unzipS1(filez,tmpdir)
    
            setl = [indir.index(i) for i in indir if (acqdate_start.split('T')[0] in i) and (absorbit in i) and (plateforme in i)]
            
            sarfiles=[]
            sarfiles2=[]
            incfiles=[]
            for ind in setl:
                
                in1vv = search_files(indir[ind], 'Sigma0_'+args.polar.upper(), 'img', 'f')
                if dblpolar:
                    in1vh = search_files(indir[ind], 'Sigma0_VH', 'img', 'f')
                    
                if args.incid == 'loc':
                    incfs = search_files(indir[ind], 'localIncidence', 'img', 'f')
                else:
                    incfs = search_files(indir[ind], 'FromEllipsoid', 'img', 'f')
                    
                if len(in1vv) !=1 :
                    print("Error: Can't find .img files in "+indir[ind])
                    continue
                else:
                    sarfiles.append(in1vv[0])
                    incfiles.append(incfs[0])
                    if dblpolar:
                        sarfiles2.append(in1vh[0])
                    dejafait.append(indir[ind])
                
    
            if not os.path.exists(outfile) or args.overwrite :        
                otberror = exmos_pipeline(sarfiles, args.inref, outfile)
                if dblpolar:
                    otberror2 = exmos_pipeline(sarfiles2, args.inref, outfile2)
                if otberror:
                    continue
            
            auxdir = os.path.join(os.path.dirname(outfile),"auxfiles")
            imgsp = os.path.basename(outfile).split("_")
             
            if args.incid == 'loc':
                out_THETA = os.path.join(auxdir,"_".join(imgsp[:-1])+"_THETALOC.TIF")
            else:
                out_THETA = os.path.join(auxdir,"_".join(imgsp[:-1])+"_THETAELLI.TIF")
            
            if not os.path.exists(auxdir):
                os.mkdir(auxdir)
                
            if not os.path.exists(out_THETA) or args.overwrite :        
                otberror = exmos_pipeline(incfiles, args.inref, out_THETA)
            
            
        procend = datetime.datetime.now()
        proctime = procend -procstart
        print("Total elapse time : "+str(proctime))
        
                    
    
class ndvi_pipeline():
    def __init__(self, args):
        self.args = args
    
    
    def process(self, args):
        
        
        def search_files(directory='.', extension='jp2', resolution='10m', band='B04'):
            images=[]
            extension = extension.lower()
            resolution = resolution.lower()
            band = band.upper()
            
            for dirpath, dirnames, files in os.walk(directory):
                for name in files:
                    if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 and name.upper().find(band) > 0:
                        
                        #print(os.path.join(dirpath, name))
                        abspath = os.path.abspath(os.path.join(dirpath, name))
                        images.append(abspath)
        
            print(str(len(images)) + " image(s) found")
            return images
        
        def ndvi_pipeline(argsformat, nir, red, mask, out):
            
            app0 = otbApplication.Registry.CreateApplication("Superimpose")
            app0.SetParameterString("inm",mask)
            app0.SetParameterString("inr",nir)
            app0.SetParameterString("out", "temp0.tif")
            app0.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
            app0.SetParameterString("interpolator", "nn")
        #    app0.SetParameterInt("ram",4000)
            app0.Execute()
            
            app1 = otbApplication.Registry.CreateApplication("BandMath")
            # Define Input im2: Band Red (B4)
        #    app1.AddParameterStringList("il",[nir,red])
            app1.AddParameterStringList("il",nir)
            app1.AddParameterStringList("il",red)
            app1.SetParameterString("out", "temp1.tif")
            app1.SetParameterString("exp", "(im1b1-im2b1)/(im1b1+im2b1)>0?(im1b1-im2b1)/(im1b1+im2b1)*100:0")
        #    app1.SetParameterInt("ram",4000)
            app1.Execute()
            
            valuemask = "0"
            if argsformat == 'S2-L3A':
                valuemask = "4"
            elif argsformat == 'L8-OLI/TIRS':
                valuemask = "322 or im2b1==386 or im2b1==834 or im2b1==898 or im2b1==1346"
                
            
            app2 = otbApplication.Registry.CreateApplication("BandMath")
            app2.AddImageToParameterInputImageList("il",app1.GetParameterOutputImage("out"))
            # Define Input im2: Band Red (B4)
            app2.AddImageToParameterInputImageList("il", app0.GetParameterOutputImage("out"))
            app2.SetParameterString("out", "temp2.tif")
            app2.SetParameterString("exp", "(im2b1=="+valuemask+")?im1b1:0")
        #    app2.SetParameterInt("ram",4000)
            app2.Execute()
            
            app3 = otbApplication.Registry.CreateApplication("ManageNoData")
            app3.SetParameterInputImage("in", app2.GetParameterOutputImage("out"))
            app3.SetParameterString("out", out+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")#
            app3.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
            app3.SetParameterString("mode", "changevalue")
        #    app3.SetParameterInt("ram",4000)
            
            app3.ExecuteAndWriteOutput()
        
        
        ##################################"
        #     MAIN
        ##################################
        
            
        if args.format == "S2-SEN2COR" :
        
            bands4=[]
            bands4=search_files(args.s2l2dir)
        
            masks=[]
            masks=search_files(args.s2l2dir, 'jp2', resolution='20m', band='CLD')
            
        elif args.format == "S2-MUSCATE" :
            
            bands4=[]
            bands4=search_files(args.s2l2dir, 'tif', resolution='SENTINEL2', band='_FRE_B4')
        
            masks=[]
            masks=search_files(args.s2l2dir, 'tif', resolution='SENTINEL2', band='_CLM_R2')
            
        elif args.format == "S2-L3A" :
            
            bands4=[]
            bands4=search_files(args.s2l2dir, 'tif', resolution='SENTINEL2', band='_FRC_B4')
        
            masks=[]
            masks=search_files(args.s2l2dir, 'tif', resolution='SENTINEL2', band='_FLG_R2')
            
        elif args.format == "L8-OLI/TIRS" :
            
            bands4=[]
            bands4=search_files(args.s2l2dir, 'tif', resolution='LC08_L1TP', band='_sr_band3')
        
            masks=[]
            masks=search_files(args.s2l2dir, 'tif', resolution='LC08_L1TP', band='_pixel_qa')
            
        else:
            print("S2 format not recognized!")
            exit
        
        if not os.path.isdir(args.ndvidir):
            os.makedirs(args.ndvidir)
        
        abspathout = os.path.abspath(args.ndvidir)
        
        for file in bands4:
    #        splitfile = file.split("/")
    #        splitfile.pop(0)
    #        bandred = "/".join(splitfile)
            bandred = file
            splitfile = bandred.upper().split("/")
            
            if args.format == "S2-SEN2COR" :
                bandnir = bandred.replace('B04', 'B08')
                ind = splitfile.index("GRANULE")
                l = [masks.index(i) for i in masks if splitfile[ind+1] in i]
                splitname = os.path.basename(bandred).split('_')
                indband = splitname.index("B04")            
                if indband == 2:
                    outfile = "NDVI_"+splitname[0]+"_"+splitname[1]+".TIF"
                elif indband == 3:
                    outfile = "NDVI_"+splitname[1]+"_"+splitname[2]+".TIF"
                else:
                    print("??????????????????????????????????????????????????????")
                    print("ERROR: "+bandred)
                    print("??????????????????????????????????????????????????????")
                    continue
                
            elif args.format == "S2-MUSCATE" or args.format == "S2-L3A" :
                bandnir = bandred.replace('B4', 'B8')
    #            ind = splitfile.index("MASKS")
                l = [masks.index(i) for i in masks if splitfile[len(splitfile)-2] in i]
                splitname = os.path.basename(bandred[:-4]).split('_')
                indband = splitname.index("B4")            
                if indband == 7:
                    imgdate = splitname[1].split("-")
                    outfile = "NDVI_"+splitname[3]+"_"+"T".join(imgdate[:2])+".TIF"
                else:
                    print("??????????????????????????????????????????????????????")
                    print("ERROR: "+bandred)
                    print("??????????????????????????????????????????????????????")
                    continue
            
            if args.format == "L8-OLI/TIRS" :
                bandnir = bandred.replace('band3', 'band5')
    #            ind = splitfile.index("GRANULE")
                l = [masks.index(i) for i in masks if splitfile[-2] in i]
                splitname = os.path.basename(bandred).split('_')
    #            indband = splitname.index("band3")            
                outfile = "NDVI_"+splitname[0]+"_"+splitname[2]+"_"+splitname[3]+".TIF"
                
           
    #        outfile = os.path.basename(bandred.replace('B04_10m.jp2', 'NDVI_10m.tif'))
            absoutfile = os.path.join(abspathout, outfile)
            
            print("###########################################################")
            print("using red = " + bandred)
            print("using nir = " + bandnir)
            print("using mask = " + masks[l[0]])
            print("using out = " + absoutfile)
            print("###########################################################")
            
            
            
            ndvi_pipeline(args.format, bandnir, bandred, masks[l[0]], absoutfile)
    

class gapfilling_pipeline():
    
    
    def __init__(self, args):
        self.args = args
    
    
    def process(self, args):
        
        def search_files(directory='.', extension='tif', resolution='NDVI'):
            images=[]
            extension = extension.lower()
            resolution = resolution.lower()
            
            for dirpath, dirnames, files in os.walk(directory):
                for name in files:
                    if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 :
                        
                        #print(os.path.join(dirpath, name))
                        abspath = os.path.abspath(os.path.join(dirpath, name))
                        images.append(abspath)
        
            print(str(len(images)) + " image(s) found")
            return images
        
        def create_mask(ndvi,mask):
        
            app1 = otbApplication.Registry.CreateApplication("BandMath")
            app1.AddParameterStringList("il",ndvi)
            app1.SetParameterString("out", mask)
            app1.SetParameterString("exp", "im1b1==0?1:0")
            app1.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
            app1.ExecuteAndWriteOutput()
        
        def concate_images(listinputimg, pixeltype, output):
            apps=[]
            for img in listinputimg:
                app0 = otbApplication.Registry.CreateApplication("Superimpose")
                app0.SetParameterString("inm", img)
                app0.SetParameterString("out", "supimp.tif")
                app0.SetParameterString("inr", listinputimg[0])
                app0.SetParameterString("interpolator", "nn")
                app0.Execute()
                apps.append(app0)
            
            print('---\nConcatenation to {}...'.format(output))
            appConcat = otbApplication.Registry.CreateApplication("ConcatenateImages")
            for a in apps: 
                appConcat.AddImageToParameterInputImageList("il",a.GetParameterOutputImage("out"))
            appConcat.SetParameterString("out", output)
            #appConcat.SetParameterOutputImagePixelType("out", pixeltype)
            appConcat.Execute()
            return appConcat
        
        def monthrange(start_date, end_date):
        #    print("MonthRange: "+str (math.floor((end_date - start_date).days/31)))
        #    for n in range(int (math.floor((end_date - start_date).days/31))+1):
        #        yield start_date + datetime.timedelta(days=n*31)
            
            qannee = end_date.year - start_date.year
            
            dmois = end_date.month +12*(qannee) - start_date.month
            
            yield start_date
            
            
            anneesuiv = start_date.year
            for m in range(dmois-1):
                m+=1
                moissuiv = (start_date.month + m) % 12
                if moissuiv == 0 :
                    moissuiv = 12
                if moissuiv == 1 :
                    anneesuiv = anneesuiv+1
                
                yield datetime.date(anneesuiv,moissuiv,1)
            
            yield end_date
        
                
        def create_outputdates(listinput_date):
        #    stdate = datetime.date(int(startdate[:4]),int(startdate[4:6]),1)
        #    eddate = datetime.date(int(enddate[:4]),int(enddate[4:6]),15)
            inst = datetime.date(int(listinput_date[0][:4]),int(listinput_date[0][4:6]),int(listinput_date[0][6:8]))
            ined = datetime.date(int(listinput_date[-1][:4]),int(listinput_date[-1][4:6]),int(listinput_date[-1][6:8]))
            
            if inst.day > 15:
                stdate = datetime.date(inst.year,inst.month +1,1)
            else:
                stdate = datetime.date(inst.year,inst.month,15)
            if ined.day >= 15:
                eddate = datetime.date(ined.year,ined.month,15)
            else:
                eddate = datetime.date(ined.year,ined.month,1)
                
            
            exformat = "%Y%m%d"
            print("Start date: "+stdate.strftime(exformat))
            print("End date: "+eddate.strftime(exformat))
            
            outputdates=[]
            #outputdates.append(stdate.strftime(exformat))
            procd = stdate
            for dstep in monthrange(stdate, eddate):
                procd = dstep
                if dstep == stdate:
                    if stdate.day == 1:
                        outputdates.append(procd.strftime(exformat))
                        procd = datetime.date(dstep.year,dstep.month,15)
                        outputdates.append(procd.strftime(exformat))
                    else:
                        outputdates.append(procd.strftime(exformat))
                elif dstep == eddate:
                    if eddate.day == 1:
                        outputdates.append(procd.strftime(exformat))
                    else:
                        procd = datetime.date(dstep.year,dstep.month,1)
                        outputdates.append(procd.strftime(exformat))
                        procd = datetime.date(dstep.year,dstep.month,15)
                        outputdates.append(procd.strftime(exformat))
                else:
                    procd = datetime.date(dstep.year,dstep.month,1)
                    outputdates.append(procd.strftime(exformat))
                    procd = datetime.date(dstep.year,dstep.month,15)
                    outputdates.append(procd.strftime(exformat))
                
            return outputdates
        
        
        ##################################"
        #     MAIN
        ##################################
        
        outdir = args.outdir
        if not os.path.exists(args.outdir):
            os.makedirs(args.outdir)
        if not os.path.exists(args.outmaskdir):
            os.makedirs(args.outmaskdir)
            
        ndvifiles=search_files(args.ndvidir)
        
        parameters=[]
        for ndvi in ndvifiles: 
            maskname = os.path.join(args.outmaskdir,os.path.basename(ndvi)[:-4]+"_MASK.TIF")
            parameters.append([os.path.basename(ndvi).split("_")[2].split("T")[0],ndvi,maskname])
            
        parameters.sort(key=lambda tup: tup[0])
        
        print("Creating MASK files from input NDVIs")
        for param in parameters:
            if os.path.exists(param[2]):
                continue
            create_mask(param[1],param[2])
        
        listinput_date = [row[0] for row in parameters]
        listinput_img = [row[1] for row in parameters]
        listinput_mask = [row[2] for row in parameters]
        
        listoutput_date = create_outputdates(listinput_date)
        
        inputdatesfile = os.path.join(args.ndvidir,"InputDates.txt")
        outputdatesfile = os.path.join(args.ndvidir,"OutputDates.txt")
        with open(inputdatesfile,'w') as indatefile:
            for text in listinput_date:
                indatefile.write(text+"\n")
                
        with open(outputdatesfile,'w') as outdatefile:
            for text in listoutput_date:
                outdatefile.write(text+"\n")
                
        
        # Concatenate
        print("Concatenate NDVI files : ")
        [print("    "+nm) for nm in listinput_img]
        #appConcatImg = concate_images(listinput_img, otbApplication.ImagePixelType_uint8, "tempimg.tif")
        apps=[]
        for img in listinput_img:
            app0 = otbApplication.Registry.CreateApplication("Superimpose")
            app0.SetParameterString("inm", img)
            app0.SetParameterString("out", "supimp.tif")
            app0.SetParameterString("inr", listinput_img[0])
            app0.SetParameterString("interpolator", "nn")
            app0.Execute()
            apps.append(app0)
        appConcatImg = otbApplication.Registry.CreateApplication("ConcatenateImages")
        for a in apps: 
            appConcatImg.AddImageToParameterInputImageList("il",a.GetParameterOutputImage("out"))
    #    appConcatImg.SetParameterStringList("il", listinput_img)
        appConcatImg.SetParameterString("out", "tempimg.tif")
        appConcatImg.Execute()
        print("Concatenate MASK files : ")
        [print("    "+nm) for nm in listinput_mask]
        #appConcatMask = concate_images(listinput_mask, otbApplication.ImagePixelType_uint8, "tempmask.tif")
        appsm=[]
        for img in listinput_mask:
            app0 = otbApplication.Registry.CreateApplication("Superimpose")
            app0.SetParameterString("inm", img)
            app0.SetParameterString("out", "supimp.tif")
            app0.SetParameterString("inr", listinput_img[0])
            app0.SetParameterString("interpolator", "nn")
            app0.Execute()
            appsm.append(app0)
        appConcatMask = otbApplication.Registry.CreateApplication("ConcatenateImages")
        for a in appsm: 
            appConcatMask.AddImageToParameterInputImageList("il",a.GetParameterOutputImage("out"))
    #    appConcatMask.SetParameterStringList("il", listinput_mask)
        appConcatMask.SetParameterString("out", "tempmask.tif")
        appConcatMask.Execute()
        
        
        # Gapfill
        print("Gapfilled NDVIs from input dates in "+inputdatesfile+" : ")
        [print("    "+nm) for nm in listinput_date]
        print("Gapfilled NDVIs to output dates in "+outputdatesfile+" : ")
        [print("    "+nm) for nm in listoutput_date]
        appGapFill = otbApplication.Registry.CreateApplication("ImageTimeSeriesGapFilling")
        appGapFill.SetParameterInputImage("in", appConcatImg.GetParameterOutputImage("out"))
        appGapFill.SetParameterInputImage("mask", appConcatMask.GetParameterOutputImage("out"))
        appGapFill.SetParameterInt("comp", 1)
        appGapFill.SetParameterString("it", args.interpolation)
        appGapFill.SetParameterString("id", inputdatesfile)
        appGapFill.SetParameterString("od", outputdatesfile)
        appGapFill.SetParameterString("out", "tempgapfill.tif")
        appGapFill.Execute()
        
        prefsp = os.path.basename(ndvifiles[0]).split('_')
        outprefix = os.path.join(args.outdir,prefsp[0]+"_"+prefsp[1])
        
        # Splitimage
        print("Spliting gapfilled image to monoband images")
        SplitImage = otbApplication.Registry.CreateApplication("SplitImage")
        SplitImage.SetParameterInputImage("in", appGapFill.GetParameterOutputImage("out"))
        SplitImage.SetParameterString("out", outprefix+".TIF")
        SplitImage.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
        SplitImage.ExecuteAndWriteOutput()
            
        # Rename image
        print("Renaming files with dates")
        i=0
        for gapdate in listoutput_date:
            os.rename(outprefix+"_"+str(i)+".TIF",outprefix+"_"+gapdate+".TIF")
            i+=1
        
        print("Done")
        
        
class stack_pipeline():
    
    def __init__(self, args):
        self.args = args
    
    
    def process(self, args):
        
        def search_files(directory='.', resolution='NDVI', extension='tif', fictype='f'):
            images=[]
            extension = extension.lower()
            resolution = resolution.lower()
            for dirpath, dirnames, files in os.walk(directory):
                if fictype == 'f':
                    for name in files:
            #            print(os.path.join(dirpath, name) + " test")
                        if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 :
                            
            #                print(os.path.join(dirpath, name) + " OK")
                            abspath = os.path.abspath(os.path.join(dirpath, name))
                            images.append(abspath)
                elif fictype == 'd':
                    for dirname in dirnames:
            #            print(os.path.join(dirpath, name) + " test")
                        if extension and dirname.lower().endswith(extension) and dirname.lower().find(resolution) >= 0 :
                            
            #                print(os.path.join(dirpath, name) + " OK")
                            abspath = os.path.abspath(os.path.join(dirpath, dirname))
                            images.append(abspath)
                else:
                    print("search_files type error")
                    exit()
                    
            return images
        
        def ndvistackandmask_pipeline(ndvis,lulc,agrivalues, out):
    
            app0 = otbApplication.Registry.CreateApplication("ConcatenateImages")
                
            app0.SetParameterStringList("il",ndvis)    
            app0.SetParameterString("out", "temp0.tif")
        #    app0.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
        #    app0.SetParameterInt("ram",4000)
            app0.Execute()
            
            
            # The following line creates an instance of the Superimpose application
            app4 = otbApplication.Registry.CreateApplication("SoilMoistureXBandLULCMask")
            
            # The following lines set all the application parameters:
            app4.SetParameterInputImage("inr", app0.GetParameterOutputImage("out"))
            app4.SetParameterString("inm", lulc)
            if agrivalues != None:
        #        params = {"labels":labels, "lulc":lulc, "agrivalues":agrivalues.split(" "), "out":maskedlabels}
                app4.SetParameterStringList("agrival", agrivalues.split(" "))
            app4.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
            app4.SetParameterString("out", out+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")
        #    app4.SetParameters(params)
            
            print("Masking NDVI stack")
            # The following line execute the applicationw
            app4.ExecuteAndWriteOutput()
            print("End of Masking \n")
        
        ##################################"
        #     MAIN
        ##################################
        
            
        if not os.path.isdir(args.ndvidir):
            print("erreur "+args.ndvidir+" n'est pas un dossier")
            exit()
        
        ndvis=[]
        ndvis=search_files(args.ndvidir)
       
        lulc = args.lulc
        agrivalues = args.agrivalues
        output = args.output
        print("Starting stack and mask pipeline")
        ndvistackandmask_pipeline(ndvis,lulc,agrivalues, output)


class slope_pipeline():
    
    def __init__(self, args):
        self.args = args
    
    
    def process(self, args):

        def unzip_hgt_file(file, outdir):
            cmd='unzip -d '+outdir+' "'+file+'"'
            print("command : "+cmd)
            p = subprocess.Popen(cmd, shell=True)
            p.wait() 

 
        ##################################"
        #     MAIN
        ##################################
        #get tiles
        raster = gdal.Open(args.inref)
        proj = osr.SpatialReference(wkt=raster.GetProjection())
        
        if proj.IsGeographic() :
            
            print("Reference image is in Geographic coordinate system. Reprojection is needed.")
            exit()

        upx, xres, xskew, upy, yskew, yres = raster.GetGeoTransform()
        cols = raster.RasterXSize
        rows = raster.RasterYSize
         
        ulx = upx + 0*xres + 0*xskew
        uly = upy + 0*yskew + 0*yres
         
        llx = upx + 0*xres + rows*xskew
        lly = upy + 0*yskew + rows*yres
         
        lrx = upx + cols*xres + rows*xskew
        lry = upy + cols*yskew + rows*yres
         
        urx = upx + cols*xres + 0*xskew
        ury = upy + cols*yskew + 0*yres
        
        outSpatialRef = osr.SpatialReference()
        outSpatialRef.ImportFromEPSG(4326)
        coordTransform = osr.CoordinateTransformation(proj, outSpatialRef)
        
        pointLL, pointUR = ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint)
        pointLL.AddPoint(llx, lly)
        pointUR.AddPoint(urx, ury)
        pointLL.Transform(coordTransform)
        pointUR.Transform(coordTransform)
        
        tilesname=[]
        print("LowerLeft="+pointLL.ExportToWkt()+";UpperRight="+pointUR.ExportToWkt())
        
        for o in range(math.ceil(pointUR.GetX()) - math.floor(pointLL.GetX())):
            for a in range(math.ceil(pointUR.GetY()) - math.floor(pointLL.GetY())):
                lat =  math.floor(pointLL.GetY()) + a
                lon =  math.floor(pointLL.GetX()) + o
                if lat >= 0 :
                    hem = 'N'
                else : 
                    hem = 'S'
                    lat = abs(lat)
                if lon >= 0 :
                    grw = 'E'
                else : 
                    grw = 'W'
                    lon = abs(lon)
                tilesname.append(hem+f'{lat:02}'+grw+f'{lon:003}')
        
        print("Unziping tiles: "+str(tilesname))
        tilefiles = []
        if len(tilesname) == 0:
            print("bug")
            exit()
            
        if args.demtype == "srtmhgt1sec":
            srtmsuf = ".SRTMGL1.hgt.zip"
        elif args.demtype == "srtm3sec":
            srtmsuf = ".SRTM.zip"
        else:
            print("wrong argument demtype!")
            exit()
            
        for tile in tilesname :
            tilef = os.path.join(args.srtmdir,tile + srtmsuf)
            if not os.path.exists(tilef):
                print("Missing SRTM tile : "+tilef)
                continue
            unzip_hgt_file(tilef,args.outdir)
            tilefiles.append(os.path.join(args.outdir,tile+".hgt"))
            
        print("Using tile files: ")
        [print(tilefile) for tilefile in tilefiles]
        outdem = os.path.join(args.outdir,"SRTM_"+args.zone+".TIF")
        mosapp = otbApplication.Registry.CreateApplication("Mosaic")
        mosparams = {"il":tilefiles, "out":"mosatemp.tif"}
        mosapp.SetParameters(mosparams)
        mosapp.Execute()
        
        supapp = otbApplication.Registry.CreateApplication("Superimpose")
        supapp.SetParameterString("inr", args.inref)
        supapp.SetParameterInputImage("inm", mosapp.GetParameterOutputImage("out"))
        supapp.SetParameterString("out", outdem)
        supapp.ExecuteAndWriteOutput()
                
        #Compute slope
        print("Generating slope file...")
        outslope = os.path.join(args.outdir,"SLOPE_"+args.zone+".TIF")
        cmd='gdaldem slope -p '+outdem+' '+outslope
        print("command : "+cmd)
        p = subprocess.Popen(cmd, shell=True)
        p.wait() 
        
        #Cleaning files
        print("Cleaning files")
        for tile in tilefiles :
            os.remove(tile)
            os.remove(tile[:-3]+"omd")
        if args.deletedem:
            os.remove(outdem)
        
        print("Done.")
        

class gpm_pipeline():
    
    def __init__(self, args):
        self.args = args
    
    
    def process(self, args):
        
        def search_files(directory='.', resolution='NDVI', extension='tif', fictype='f'):
            images=[]
            extension = extension.lower()
            resolution = resolution.lower()
            for dirpath, dirnames, files in os.walk(directory):
                if fictype == 'f':
                    for name in files:
            #            print(os.path.join(dirpath, name) + " test")
                        if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 :
                            
            #                print(os.path.join(dirpath, name) + " OK")
                            abspath = os.path.abspath(os.path.join(dirpath, name))
                            images.append(abspath)
                elif fictype == 'd':
                    for dirname in dirnames:
            #            print(os.path.join(dirpath, name) + " test")
                        if extension and dirname.lower().endswith(extension) and dirname.lower().find(resolution) >= 0 :
                            
            #                print(os.path.join(dirpath, name) + " OK")
                            abspath = os.path.abspath(os.path.join(dirpath, dirname))
                            images.append(abspath)
                else:
                    print("search_files type error")
                    exit()
                    
            return images
        
        def get_meteodates_from_sardate(sardate):
            exformat = "%Y%m%d"
            saryear,sarmonth,sarday = sardate[:4],sardate[4:6],sardate[6:8]
            fsardate = datetime.date(int(saryear),int(sarmonth),int(sarday))
            oneday = datetime.timedelta(days=1)
            sardatem1 = (fsardate - oneday).strftime(exformat)
            sardatem2 = (fsardate - oneday - oneday).strftime(exformat)
            return [sardate,sardatem1,sardatem2]
        
        def download_gpm(sarlist, sartype, gpmdir):
            datepos=None
            if sartype == "snap":
                print("#date position")
                datepos=4
            elif sartype == "s2tile":
                datepos=4
            else:
                print("Error on sartype!")
                exit()
            
            if not os.path.exists(gpmdir):
                os.mkdir(gpmdir)
                
            cmd = "wget --user=loic.lozach@irstea.fr --password=loic.lozach@irstea.fr \
                    ftp://arthurhou.pps.eosdis.nasa.gov/gpmdata/"
            arg0 = "wget"
            arg1 = "--user=loic.lozach@irstea.fr"
            arg2 = "--password=loic.lozach@irstea.fr"
            arg3 = "ftp://arthurhou.pps.eosdis.nasa.gov/gpmdata/"
            
            for sar in sarlist:
                sardata = os.path.basename(sar)
                sarsplit = sardata.split('_')
                sardate = sarsplit[datepos][:8]
                sar3dates = get_meteodates_from_sardate(sardate)
                
                tifexits = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"-S000000-E235959*.tif"))
                if len(tifexits) >= 1 :
                    continue
                
                tifexitsm1 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"-S000000-E235959*.tif"))
                if len(tifexitsm1) >= 1 :
                    continue
                
                tifexitsm2 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"-S000000-E235959*.tif"))
                if len(tifexitsm2) >= 1 :
                    continue
                
                
                gpmtif = sar3dates[0][:4] +"/"+ sar3dates[0][4:6] +"/"+ sar3dates[0][6:8] +"/gis/"+ "3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"-S000000-E235959*.t*"
                gpmtifm1 = sar3dates[1][:4] +"/"+ sar3dates[1][4:6] +"/"+ sar3dates[1][6:8] +"/gis/"+  "3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"-S000000-E235959*.t*"
                gpmtifm2 = sar3dates[2][:4] +"/"+ sar3dates[2][4:6] +"/"+ sar3dates[2][6:8] +"/gis/"+  "3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"-S000000-E235959*.t*"
                cmdlist=[]
                cmdlist.append([arg0,arg1,arg2,arg3 + gpmtif])
                cmdlist.append([arg0,arg1,arg2,arg3 + gpmtifm1])
                cmdlist.append([arg0,arg1,arg2,arg3 + gpmtifm2])
                
                for c in cmdlist:
                    p = Popen(c, cwd=gpmdir, stdout=PIPE)
                    output = p.communicate()[0]
                    if p.returncode != 0: 
                        print("wget failed %d : %s" % (p.returncode, output))
                        
        def process_gpm(sar, sar3dates, gpmdir):
            outdir = os.path.join( os.path.dirname(gpmdir),"MaskWetOrDry")
            outfile = os.path.join(outdir,"MaskWetOrDry"+args.zone+"_"+sar3dates[0]+".tif")
            if os.path.exists(outfile):
                print("Already exists : "+outfile)
                return
            if not os.path.exists(outdir):
                os.mkdir(outdir)
                
            gpmfiles1 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"*.tif"))
            gpmfiles2 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"*.tif"))
            gpmfiles3 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"*.tif"))
            
            if  len(gpmfiles1) == 0 :
                print("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"*.tif")
                return 1
            if  len(gpmfiles2) == 0 :
                print("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"*.tif")
                return 1
            if  len(gpmfiles3) == 0 :
                print("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"*.tif")
                return 1
            
            gpmfiles=[gpmfiles1[0],gpmfiles2[0],gpmfiles3[0]]
            apps=[]
            for f in gpmfiles :
                
                app = otbApplication.Registry.CreateApplication("Superimpose")
                
                app.SetParameterString("inm", f)
                app.SetParameterString("inr", sar)
                app.SetParameterString("interpolator","bco")
                app.SetParameterString("out", "temp2.tif")
                
                app.Execute() #ExecuteAndWriteOutput() 
                apps.append(app)
            
            app0 = otbApplication.Registry.CreateApplication("BandMath")
            app0.AddImageToParameterInputImageList("il", apps[0].GetParameterOutputImage("out"))
            app0.AddImageToParameterInputImageList("il", apps[1].GetParameterOutputImage("out"))
            app0.AddImageToParameterInputImageList("il", apps[2].GetParameterOutputImage("out"))
            app0.SetParameterString("out", outfile)
            app0.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
            app0.SetParameterString("exp", "im1b1+im2b1+im3b1>5?2:1")
            app0.ExecuteAndWriteOutput()
            
            
        def process_gpm_noresampling(sar, sar3dates, gpmdir):
            outdir = os.path.join( os.path.dirname(gpmdir),"CumulPluie")
            outfile = os.path.join(outdir,"CumulPluie_"+args.zone+"_"+sar3dates[0]+".tif")
            if os.path.exists(outfile):
                print("Already exists : "+outfile)
                return
            
            if not os.path.exists(outdir):
                os.mkdir(outdir)
                
            gpmfiles1 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"*.tif"))
            gpmfiles2 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"*.tif"))
            gpmfiles3 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"*.tif"))
            
            if  len(gpmfiles1) == 0 :
                print("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"*.tif")
                return 1
            if  len(gpmfiles2) == 0 :
                print("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"*.tif")
                return 1
            if  len(gpmfiles3) == 0 :
                print("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"*.tif")
                return 1
            
            gpmfiles=[gpmfiles1[0],gpmfiles2[0],gpmfiles3[0]]
            apps=[]
            for f in gpmfiles :
                
                app = otbApplication.Registry.CreateApplication("ExtractROI")
                
                app.SetParameterString("in", f)
                app.SetParameterString("mode.fit.im", sar)
                app.SetParameterString("mode","fit")
                app.SetParameterString("out", "temp2.tif")
                
                app.Execute() #ExecuteAndWriteOutput() 
                apps.append(app)
            
            app0 = otbApplication.Registry.CreateApplication("BandMath")
            app0.AddImageToParameterInputImageList("il", apps[0].GetParameterOutputImage("out"))
            app0.AddImageToParameterInputImageList("il", apps[1].GetParameterOutputImage("out"))
            app0.AddImageToParameterInputImageList("il", apps[2].GetParameterOutputImage("out"))
            app0.SetParameterString("out", outfile)
            app0.SetParameterString("exp", "im1b1+im2b1+im3b1")
            app0.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint16)
            app0.ExecuteAndWriteOutput()
            
        #     app1 = otbApplication.Registry.CreateApplication("Smoothing")
        #     app1.SetParameterInputImage("in", app0.GetParameterOutputImage("out"))
        #     app1.SetParameterString("type", "gaussian")
        #     app1.SetParameterString("out", outfile)
        #     app1.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint16)
        #     app1.ExecuteAndWriteOutput()
            
        def select_tfmodel_from_gpm(sardate,gpmdir):
            cumul = glob.glob(os.path.join( os.path.join( os.path.dirname(gpmdir),"CumulPluie"), "CumulPluie_"+args.zone+"_"+sardate+".tif"))
            
            if len(cumul) != 1 :
                print("Error : Can't find file "+"CumulPluie_"+args.zone+"_"+sardate+".tif")
                exit()
                
            raster = gdal.Open(cumul[0])
            srcband = raster.GetRasterBand(1)
            stats = gdal.Band.GetStatistics(srcband,1,1)
            
            if stats[2] > 5 :
                return str(stats[2]),"wet"
            else :
                return str(stats[2]),"dry"    
         
        ##################################"
        #     MAIN
        ##################################
        sars=[]
        datepos=None
        if args.sardirtype == "snap":
            sars=search_files(args.sardir, 'S1', 'data', 'd')
            datepos=4
        elif args.sardirtype == "s2tile":
            datepos=4
            if args.sarmode == "vv":
                sars=search_files(args.sardir, 'S1', 'VV.TIF', 'f')
            elif args.sarmode == "vh":
                sars=search_files(args.sardir, 'S1', 'VH.TIF', 'f')
            else:
                print("Error: Exception on sarmode")
                exit()
        else:
            print("ERROR: wrong sardirtype argument")
            exit()
        
        download_gpm(sars, args.sardirtype, args.gpmdir)
        affectation=[]
        with open(args.outtxt, 'w') as affout:
            for sar in sars :
                sardata = os.path.basename(sar)
                sarsplit = sardata.split('_')
                sardatetime = sarsplit[datepos]
                sardate = sardatetime.split("T")[0]
                sar3dates = get_meteodates_from_sardate(sardate)
                
                if args.resampling :
                    process_gpm(sar, sar3dates, args.gpmdir)
                else:
                    process_gpm_noresampling(sar, sar3dates, args.gpmdir)
                
                res=select_tfmodel_from_gpm(sardate, args.gpmdir)
                
                affout.write(sar+";"+res[0]+";"+res[1]+"\n")

class processrgp_pipeline():
    
        
    def __init__(self, args):
        self.args = args
    
    
    def process(self, args):
        
        def process_command(cmd):
            print("Starting : "+" ".join(cmd))
            p = Popen(cmd, stdout=PIPE)
        #    p.wait()
            output = p.communicate()[0]
            if p.returncode != 0: 
                print("process failed %d : %s" % (p.returncode, output))
                exit()
            print("#################################################")
            return p.returncode

        def rasterize(clip,ref,lfield, out):
        
            app1 = otbApplication.Registry.CreateApplication("Rasterization")
            app1.SetParameterString("in",clip)
            app1.SetParameterString("out", out)
            app1.SetParameterString("im", ref)
            app1.SetParameterString("mode", "attribute")
            app1.SetParameterString("mode.attribute.field", lfield)
            app1.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint32)
            app1.ExecuteAndWriteOutput()
            
        def create_mask(labels,mask):
        
            app1 = otbApplication.Registry.CreateApplication("BandMath")
            app1.AddParameterStringList("il",labels)
            app1.SetParameterString("out", mask)
            app1.SetParameterString("exp", "im1b1>0?1:0")
            app1.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
            app1.ExecuteAndWriteOutput()
        
        def agri_filtering(labels,lulc,agrivalues, maskedlabels):
        
            app4 = otbApplication.Registry.CreateApplication("SoilMoistureSegmentationFiltering")
            params = {"labels":labels, "lulc":lulc, "agrivalues":agrivalues.split(" "), "out":maskedlabels}
            app4.SetParameters(params)
            
            print("Masking segmentation labels")
            # The following line execute the applicationw
            app4.ExecuteAndWriteOutput()
            print("End of Masking \n")
            
        def get_clipsrc(imgref, rgpshp):
            raster = gdal.Open(imgref)
            proj = osr.SpatialReference(wkt=raster.GetProjection())
            #print("ref: "+raster.GetProjection())
            upx, xres, xskew, upy, yskew, yres = raster.GetGeoTransform()
            cols = raster.RasterXSize
            rows = raster.RasterYSize
             
            ulx = upx + 0*xres + 0*xskew
            uly = upy + 0*yskew + 0*yres
             
            llx = upx + 0*xres + rows*xskew
            lly = upy + 0*yskew + rows*yres
             
            lrx = upx + cols*xres + rows*xskew
            lry = upy + cols*yskew + rows*yres
             
            urx = upx + cols*xres + 0*xskew
            ury = upy + cols*yskew + 0*yres
            
            pointLL, pointUR = ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint)
            pointLL.AddPoint(llx, lly)
            pointUR.AddPoint(urx, ury)
            # Create ring
            ring = ogr.Geometry(ogr.wkbLinearRing)
            ring.AddPoint(ulx, uly)
            ring.AddPoint(llx, lly)
            ring.AddPoint(lrx, lry)
            ring.AddPoint(urx, ury)
            ring.AddPoint(ulx, uly)
            
            # Create polygon
            poly = ogr.Geometry(ogr.wkbPolygon)
            poly.AddGeometry(ring)
            
            if ".shp" in rgpshp:
                drvname = 'ESRI Shapefile'
            elif ".gpkg" in rgpshp:
                drvname = 'GPKG'
            else:
                print("ERROR: wrong vector type")
                exit()
                
            driver = ogr.GetDriverByName(drvname)

            dataset = driver.Open(rgpshp)
            layer = dataset.GetLayer()
            rgpprog = layer.GetSpatialRef()
            #print("before:"+poly.ExportToWkt())
            #print("RGP: "+rgpprog.ExportToWkt())

            if not proj.IsSame(rgpprog) :
                coordTransform = osr.CoordinateTransformation(proj,rgpprog)
                poly.Transform(coordTransform)
                #print("after:"+poly.ExportToWkt())
            
            return poly.ExportToWkt()
    
        ##################################"
        #     MAIN
        ##################################
        
        if not os.path.exists(args.outdir):
            os.makedirs(args.outdir)
        
        print("Clipping RPG shapefile...")
        outshp = os.path.join(args.outdir,"RGP_"+args.zone+".shp")
        if not os.path.exists(outshp):
            cmd=['ogr2ogr', '-clipsrc', get_clipsrc(args.inref, args.rgpshp), outshp, args.rgpshp]
            process_command(cmd)
        
        print("Rasterizing clip...")
        outlabels = os.path.join(args.outdir,"RGP_"+args.zone+".TIF")
        if not os.path.exists(outlabels):
            rasterize(outshp,args.inref,args.labelsfield, outlabels)
        
        print("Creating agricultural mask...")
        outmask = os.path.join(args.outdir,"RGP_"+args.zone+"_MASK.TIF")
        if not os.path.exists(outmask):
            create_mask(outlabels,outmask)
        
        print("Filtering labels...")
        outmaskedlabels = os.path.join(args.outdir,"MASKEDLABELS_"+args.zone+".TIF")
        agri_filtering(outlabels,outmask,"1", outmaskedlabels)
        
        print("Done.")

if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Preprocessing Services
        """)

    subparsers = parser.add_subparsers(help='Preprocess pipeline', dest="pipeline")

    # Short pipeline
    list_parser = subparsers.add_parser('exmosS1', help="Extract and mosaic SNAP calibrated Sentinel-1 to fit Sentinel-2 Tile")
    list_parser.add_argument('-indir', action='store', required=True, help='Directory containing Sentinel1 SNAP Calibrated .data directories')
    list_parser.add_argument('-inref', action='store', required=True, help='Image references Sentinel-2 Tile, example any S2 NDVI')
    list_parser.add_argument('-zone', action='store', required=True, help='Geographic zone reference for output files naming')
    list_parser.add_argument('-polar', choices=['vv','vh', 'vvvh'], required=False, default="vv", help="[Optional] Polarization to process, vv, vh or both, default vv")
    list_parser.add_argument('-incid', choices=['loc','elli'], required=False, default="loc", help="[Optional] Local or from ellipsoid incidence image to process, default loc")
    list_parser.add_argument('-nodem', dest='dem', action='store_false',default=True, required=False, help='[Optional] No DEM and SLOPE file generation (default True)')
    list_parser.add_argument('-outdir', action='store', required=True, help='Output directory')
    list_parser.add_argument('-overwrite', dest='overwrite', action='store_true',required=False, help='[Optional] Overwrite already existing files (default False)')
    list_parser.set_defaults(overwrite=False)

    list_parser = subparsers.add_parser('ndvi', help="Compute cloud-masked NDVI from Sentinel2 or Landsat8.")
    list_parser.add_argument('-s2l2dir', action='store', required=True, help='Directory containing Sentinel2 L2A dimap directories')
    list_parser.add_argument('-format', choices=['S2-MUSCATE','S2-SEN2COR','S2-L3A','L8-OLI/TIRS'], required=True)
    list_parser.add_argument('-ndvidir', action='store', required=True, help='Output directory for computed NDVIs')
    
    list_parser = subparsers.add_parser('gapfilling', help="Perform Gapfilling over NDVI time serie, W")
    list_parser.add_argument('-ndvidir', action='store', required=True, help='Directory containing time serie NDVI to be gapfilled')
    list_parser.add_argument('-outmaskdir', action='store', required=True, help='Output directory for NDVIs mask used for gapfilling')
    list_parser.add_argument('-interpolation', choices=['linear', 'spline'], default='linear', required=False, help='Interpolation mode for OTB ImageTimeSeriesGapFilling')
    list_parser.add_argument('-outdir', action='store', required=True, help='Output directory for time serie gapfilled NDVIs image')
    
    # RGP
    list_parser = subparsers.add_parser('processrgp', help="Create plot label image and agricultural mask from RGP")
    list_parser.add_argument('-rgpshp', action='store', required=True, help='RGP (Reseau Graphique Parcellaire) shapefile or geopackage')
    list_parser.add_argument('-labelsfield', action='store', required=False, help='[Optional] Labels field name , default ID_PARCEL')
    list_parser.set_defaults(labelsfield="ID_PARCEL")
    list_parser.add_argument('-inref', action='store', required=True, help='Image reference for RGP extraction')
    list_parser.add_argument('-zone', action='store', required=True, help='Geographic Sentinel2 zone reference for output files naming')
    list_parser.add_argument('-outdir', action='store', required=True, help='Output directory for masked labels images')
    
    
    # Stack and mask before segm pipeline
    list_parser = subparsers.add_parser('stack', help="Create stacked and masked image from NDVI directory to the input of a segmentation process")
    list_parser.add_argument('-ndvidir', action='store', required=True, help='Directory to find all the NDVI file to stack. Files naming must contain "NDVI" and ".tif" extension')
    list_parser.add_argument('-lulc', action='store', required=True, help='LandUseLandCover raster to be used for labels masking (Default THEIA OSO)')
    list_parser.add_argument('-agrivalues', action='store', required=False, help='[Optional] List of agricultural areas values in LandUseLandCover raster, default "11 12"')
    list_parser.add_argument('-output', action='store', required=True, help='Stacked and masked NDVIs output file name')
    
    # Extract SRTM1SecHGT from Snap download and compute slope
    list_parser = subparsers.add_parser('slope', help="Extract SRTM1SecHGT with image ref and compute slope")
    list_parser.add_argument('-srtmdir', action='store', required=True, help='DEM SRTM 1Sec HGT directory containing zip file')
    list_parser.add_argument('-inref', action='store', required=True, help='Image reference for DEM extraction in projected reference system')
    list_parser.add_argument('-zone', action='store', required=True, help='Geographic Sentinel2 zone reference for output files naming')
    list_parser.add_argument('-demtype', choices=['srtmhgt1sec', 'srtm3sec'],  default='srtmhgt1sec', required=False, help='[Optional] Choose between SRTM HGT 1sec or SRMT 3sec, default srtmhgt1sec')
    list_parser.add_argument('--no-deletedem', dest='deletedem', action='store_false', help='Avoid deleting DEM file used to produce slope (default True)')
    list_parser.set_defaults(deletedem=True)
    list_parser.add_argument('-outdir', action='store', required=True, help='Output directory for slope image')
    
    # Select wet or dry model for inversion from GPM data
    #WARNING: adresse gpm nasa a modifier (voir mail actu gpm) 
    list_parser = subparsers.add_parser('gpm', help="Downloads, extracts and computes synthesis of NASA Global Precipitation Model raster for TF model selection (wet or dry)")
    list_parser.add_argument('-sardir', action='store', required=True, help='Directory to find Sentinel-1 images')
    list_parser.add_argument('-sardirtype', choices=['snap', 's2tile'],  default='s2tile', required=False, help='[Optional] Choose between Sentinel-1 images processed by ESA-SNAP software ("Sigma0_VV.img" and "incidenceAngleFromEllipsoid.img" in .data directory) \
                                                                                                                 AND Sentinel-1 images processed by SoilMoistureBatchExtractAndMosaic2S2Tile.py')
    list_parser.add_argument('-sarmode', choices=['vv', 'vh'],  default='vv', required=False, help='[Optional] Choose between VV or VH mode for input SAR image, default vv')
    list_parser.add_argument('-gpmdir', action='store', required=True, help='Directory where GPM data will be downloaded, autodetect previous downloads')
    list_parser.add_argument('-zone', action='store', required=True, help='Geographic zone reference for output files naming')
    list_parser.add_argument('-outtxt', action='store', required=True, help='Output text file where each Sentinel-1 dates correspond their model dry or wet to use')
    list_parser.add_argument('--no-resampling', dest='resampling', action='store_false', help='[Optional] Resample GPM 100km to 10m, default false')
    list_parser.set_defaults(resampling=False)
    
    args=parser.parse_args()
    

    if args.pipeline == 'processrgp' or args.pipeline == 'slope' or args.pipeline == 'gpm' or args.pipeline == 'exmosS1':
        if args.zone != None and args.zone.find("_") >= 0 :
            print("Error: '_' character is forbidden in -zone string")
            exit()
        
    if args.pipeline == 'exmosS1' :
        print("Starting Extract and Mosaic Sentinel-1")
        p = exmosaic_pipeline(args)
        p.process(args)
        
    elif args.pipeline == 'ndvi' :
        print("Starting NDVI computation")
        p = ndvi_pipeline(args)
        p.process(args)
        
    elif args.pipeline == 'gapfilling' :
        print("Starting NDVI Gapfilling")
        p = gapfilling_pipeline(args)
        p.process(args)
    
    elif args.pipeline == 'processrgp' :
        print("Starting RGP processing")
        p = processrgp_pipeline(args)
        p.process(args)
            
    elif args.pipeline == 'stack' :
        print("Starting Stack NDVI")
        p = stack_pipeline(args)
        p.process(args)

    elif args.pipeline == 'slope' :
        print("Starting slope generation")
        p = slope_pipeline(args)
        p.process(args)

    elif args.pipeline == 'gpm' :
        print("Starting Model Choice from GPM")
        p = gpm_pipeline(args)
        p.process(args)

    else:
        print("Error: wrong pipeline argument "+ args.pipeline )
        parser.exit()
        
        
        
        
        
        