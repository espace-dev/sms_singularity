#!/usr/bin/python
import argparse
import glob
import os
# from subprocess import Popen, PIPE
import subprocess
import csv
import rabbitmq_log

# def process_pipeline(cmd_list):
#     for cmd in cmd_list:
#         print("Starting : " + " ".join(cmd))
#
#         p = subprocess.Popen(cmd, stdout=subprocess.PIPE, cwd=os.getcwd())
#         # p = Popen(cmd, stdout=PIPE, cwd=wd)
#         #    p.wait()
#         output = p.communicate()[0]
#         if p.returncode != 0:
#             print("process failed %d : %s" % (p.returncode, output))
            # exit()
        # print(output)
        # return p.returncode

def is_theia_tile(tile):
    with open('/work/python/data/S2_THEIA/S2L3ATHEIA.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in spamreader:
            if tile == ''.join(row):
                return 1



if __name__ == "__main__":

    parser = argparse.ArgumentParser(description=
                                     """
        Full soil moisture map services pipeline
        """)

    parser.add_argument('-tile', action='store', required=True, help="Sentinel2 Tile name ex: T30UVU")
    parser.add_argument('-start', action='store', required=True, help='Starting date, ex. 2019-01-01')
    parser.add_argument('-end', action='store', required=True, help='Ending date, ex. 2019-12-31')
    parser.add_argument('-outdir', action='store', required=True, help='Output directory')  # /data/out_directory
    parser.add_argument('-host', action='store', default='localhost', help='RabbitMQ host')
    parser.add_argument('-queue', action='store', default='SMS', help='RabbitMQ queue')
    parser.add_argument('-exchange', action='store', default='', help='RabbitMQ exchange')
    parser.add_argument('-key', action='store', default='SMS', help='RabbitMQ key')
    args = parser.parse_args()

    outabs = os.path.abspath(args.outdir)
    #rabbitmq_log.rmq_log(host='localhost', queue='hello', exchange='', key='hello', message='Start processing')

    # s2dl_cmd = ['download_service.py', 'theia', '-tile', args.tile, '-start', args.start, '-end', args.end,
    #            '-outdir', outabs + '/Sentinel2']
    # subprocess.run(s2dl_cmd, cwd=os.getcwd())
    #
    # s1dl_cmd = ['download_service.py', 'peps', '-tile', args.tile, '-start', args.start, '-end', args.end,
    #            '-outdir', outabs + '/Sentinel1']
    # subprocess.run(s1dl_cmd, cwd=os.getcwd())

    if is_theia_tile(args.tile):
         preprocessndvi_cmd = ['preprocess_service.py', 'ndvi', '-s2l2dir', outabs + '/S2_tmp', '-format', 'S2-L3A',
                              '-ndvidir', outabs + '/NDVI']
    else:
        preprocessndvi_cmd = ['preprocess_service.py', 'ndvi', '-s2l2dir', outabs + '/S2_tmp', '-format', 'S2-SEN2COR',
                          '-ndvidir', outabs + '/NDVI']

    # rabbitmq_log.rmq_log(host=args.host, queue=args.queue, exchange=args.exchange, key=args.key,
    #                   message='Running preprocess ndvi')
    subprocess.run(preprocessndvi_cmd, cwd=os.getcwd())
    # rabbitmq_log.rmq_log(host=args.host, queue=args.queue, exchange=args.exchange, key=args.key,
    #                   message='Preprocess ndvi DONE')

    # rabbitmq_log.rmq_log(host=args.host, queue=args.queue, exchange=args.exchange, key=args.key,
    #                   message='Running preprocess gapfilling')
    preprocessgf_cmd = ['preprocess_service.py', 'gapfilling', '-ndvidir', outabs + '/NDVI',
                        '-outmaskdir', outabs + '/OUTMASKDIR', '-interpolation', 'linear',
                        '-outdir', outabs + '/GAPFILLED']
    subprocess.run(preprocessgf_cmd, cwd=os.getcwd())
    # rabbitmq_log.rmq_log(host=args.host, queue=args.queue, exchange=args.exchange, key=args.key,
    #                   message='Preprocess gapfilling DONE')

    # select first NDVI file for the tile
    inref_file = os.path.abspath(glob.glob(outabs + '/GAPFILLED/*' + args.tile + '*.TIF')[0])

    # rabbitmq_log.rmq_log(host=args.host, queue=args.queue, exchange=args.exchange, key=args.key,
    #                   message='Running preprocess RGP')
    RGP_file = os.path.abspath(glob.glob(outabs + '/RGP/*.shp')[0])
    preprocessrgp_cmd = ['preprocess_service.py', 'processrgp', '-rgpshp', RGP_file, '-inref', inref_file,
                         '-zone', args.tile, '-outdir', outabs + '/MASKED']
    subprocess.run(preprocessrgp_cmd, cwd=os.getcwd())
    # rabbitmq_log.rmq_log(host=args.host, queue=args.queue, exchange=args.exchange, key=args.key,
    #                   message='Preprocess RGP DONE')
    #
    # rabbitmq_log.rmq_log(host=args.host, queue=args.queue, exchange=args.exchange, key=args.key,
    #                   message='Running calibration service')
    calib_cmd = ['calibration_service.py', '-indir', outabs + '/S1_tmp', '-inref', inref_file,
                 '-zone', args.tile, '-outdir', outabs + '/S1cal']
    subprocess.run(calib_cmd, cwd=os.getcwd())
    print("Cleaning smallest files")
    for files in glob.glob(outabs + '/S1cal/*.TIF'):
        if os.path.isfile(files): head = os.path.splitext(files)[0]
        t = head + '.TIF'
        g = head + '.geom'
        if os.path.getsize(files) <= 100*1000000:
            os.remove(t)
            os.remove(g)
    # rabbitmq_log.rmq_log(host=args.host, queue=args.queue, exchange=args.exchange, key=args.key,
    #                   message='Calibration service DONE')

    maskedlabels = os.path.abspath(glob.glob(outabs + '/MASKED/MASKEDLABELS_' + args.tile + '*.TIF')[0])

    # rabbitmq_log.rmq_log(host=args.host, queue=args.queue, exchange=args.exchange, key=args.key,
    #                   message='Running production service')
    prod_cmd = ['production_service.py', 'serie', '-modeldir', outabs + '/ModelSMS', '-sardir', outabs + '/S1cal',
                '-ndvidir', outabs + '/GAPFILLED', '-maskedlabels', maskedlabels, '-zone', args.tile,
                '-outformat', 'raster', '-outdir', outabs + '/SMM']
    subprocess.run(prod_cmd, cwd=os.getcwd())
    # rabbitmq_log.rmq_log(host=args.host, queue=args.queue, exchange=args.exchange, key=args.key,
    #                   message='Production service DONE')

    # cmd_list = [s2dl_cmd, s1dl_cmd, preprocessndvi_cmd, preprocessgf_cmd, preprocessrgp_cmd, calib_cmd, prod_cmd]
    # process_pipeline(cmd_list)
