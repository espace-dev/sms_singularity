#!/usr/bin/python
import argparse
import glob
import os
import subprocess
import shutil
import csv

# def process_pipeline(cmd_list):
#     for cmd in cmd_list:
#         print("Starting : " + " ".join(cmd))
#
#         p = subprocess.Popen(cmd, stdout=subprocess.PIPE, cwd=os.getcwd())
#         # p = Popen(cmd, stdout=PIPE, cwd=wd)
#         #    p.wait()
#         output = p.communicate()[0]
#         if p.returncode != 0:
#             print("process failed %d : %s" % (p.returncode, output))
# exit()
# print(output)
# return p.returncode

data_dir = "/work/python/"


def is_theia_tile(tile):
    with open(data_dir + 'data/S2_THEIA/S2L3ATHEIA.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in spamreader:
            if tile == ''.join(row):
                return 1



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=
                                     """
        Download S2 and S1 images and transfer json
        """)

    parser.add_argument('-tile', action='store', required=True, help="Sentinel2 Tile name ex: T30UVU")
    parser.add_argument('-start', action='store', required=True, help='Starting date, ex. 2019-01-01')
    parser.add_argument('-end', action='store', required=True, help='Ending date, ex. 2019-12-31')
    parser.add_argument('-outdir', action='store', required=True, help='Output directory')  # /data/out_directory
    args = parser.parse_args()

    outabs = os.path.abspath(args.outdir)

    if is_theia_tile(args.tile):
        s2dl_cmd = ['download_service.py', 'theia', '-tile', args.tile, '-start', args.start, '-end', args.end,
                    '-outdir', outabs + '/Sentinel2']
    else:
        s2dl_cmd = ['download_service.py', 'scihub', '-tile', args.tile, '-start', args.start, '-end', args.end,
                    '-outdir', outabs + '/Sentinel2']
    subprocess.run(s2dl_cmd, cwd=os.getcwd())

    s1dl_cmd = ['download_service.py', 'peps', '-tile', args.tile, '-start', args.start, '-end', args.end,
                '-outdir', outabs + '/Sentinel1']
    subprocess.run(s1dl_cmd, cwd=os.getcwd())
    shutil.copyfile("/work/python/peps_download/search.json", outabs + "/search.json")
