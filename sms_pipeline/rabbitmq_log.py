#!/usr/bin/env python3
"""
RabbitMQ module to send messages
"""
import pika
import argparse


def rmq_log(host='localhost', queue='hello', exchange='', key='hello', message='test'):
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        host=host))
    channel = connection.channel()

    channel.queue_declare(queue=queue)

    channel.basic_publish(exchange='',
                          routing_key=key,
                          body=message)
    print(" [x] Sent " + message)
    connection.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=
                                     """
                                     RabbitMQ connection
                                     """)

    parser.add_argument('-host', action='store', required=True, help='Host IP')
    parser.add_argument('-queue', action='store', required=True,
                        help='Queue name')
    parser.add_argument('-exchange', action='store', required=True,
                        help='Exchange key')
    parser.add_argument('-key', action='store', required=True, help='Routing key')
    parser.add_argument('-message', action='store', required=True, help='Message to send')

    parser.set_defaults(overwrite=False)

    args = parser.parse_args()

    overwrite = args.overwrite

    rmq_log(args.host, args.queue, args.exchange, args.key, args.message)