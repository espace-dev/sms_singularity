#!/usr/bin/python
import os, sys, argparse, glob
from zipfile import ZipFile
# sys.path.append("/work/python")
# from SentinelDM import SenTUI as tui
import geopandas as gpd
# from osgeo import ogr
from subprocess import Popen, PIPE


# sq = r"""shp << '/data/S2Tiles/T30UVU.shp';
# aoi in shp;
# bp from NOW to NOW-3d;
# pfname = s1;
# ptype = GRD;
# """

def process_command(cmd, wd):
    #     my_env = os.environ.copy()
    #     my_env["PATH"] = "/work/python/theia_download:" + my_env["PATH"]    , env=my_env
    print(cmd)
    # print("Starting : " + " ".join(cmd))

    # p = Popen(cmd, stdout=PIPE, cwd="/work/python/theia_download")
    p = Popen(cmd, stdout=PIPE, cwd=wd)
    #    p.wait()
    output = p.communicate()[0]
    if p.returncode != 0:
        print("process failed %d : %s" % (p.returncode, output))
        # exit()
    print(output)
    # return p.returncode


def s2_tile_centroid(tile):
    shp = gpd.read_file("/work/python/data/S2Tiles/S2Tiles.shp")
    x_centroid = shp.xc.values[shp['Name'] == tile]
    y_centroid = shp.yc.values[shp['Name'] == tile]
    return [x_centroid.item(), y_centroid.item()]


if __name__ == "__main__":

    # Make parser object
    parser = argparse.ArgumentParser(description=
                                     """
        Download services
        """)

    subparsers = parser.add_subparsers(help='Choose server', dest="pipeline")

    # Short pipeline
    # list_parser = subparsers.add_parser('scihub', help="Download from Copernicus Scihub server")

    list_parser = subparsers.add_parser('theia', help="Download S2 L3A from Theia-Land server")
    list_parser.add_argument('-tile', action='store', required=True, help="Sentinel2 Tile name ex: T30UVU")
    list_parser.add_argument('-start', action='store', required=True, help='Starting date, ex. 2019-01-01')
    list_parser.add_argument('-end', action='store', required=True, help='Ending date, ex. 2019-12-31')
    list_parser.add_argument('-outdir', action='store', required=True, help='Output directory')

    list_parser = subparsers.add_parser('scihub', help="Download S2 L2A from Scihub server")
    list_parser.add_argument('-tile', action='store', required=True, help="Sentinel2 Tile name ex: T30UVU")
    list_parser.add_argument('-start', action='store', required=True, help='Starting date, ex. 20190101')
    list_parser.add_argument('-end', action='store', required=True, help='Ending date, ex. 20191231')
    list_parser.add_argument('-outdir', action='store', required=True, help='Output directory')

    list_parser = subparsers.add_parser('peps', help="Download S1 from CNES Peps server")
    list_parser.add_argument('-tile', action='store', required=True, help="Sentinel2 Tile name ex: T30UVU")
    list_parser.add_argument('-start', action='store', required=True, help='Starting date, ex. 2019-01-01')
    list_parser.add_argument('-end', action='store', required=True, help='Ending date, ex. 2019-12-31')
    list_parser.add_argument('-outdir', action='store', required=True, help='Output directory')

    args = parser.parse_args()

    # if args.pipeline == 'scihub' :
    #     os.system('mode con: cols={} lines={}'.format(140,40))
    #     main = tui.MainTUI(query_string=sq) #query_string=sq
    #     main.run()
    if args.pipeline == 'theia':
        wd = "/work/python/theia_download"
        outabs = os.path.abspath(args.outdir)
        if not os.path.exists(outabs):
            os.makedirs(outabs)

        cmd = ['python', 'theia_download.py', '-t', args.tile, '-a', 'config_theia.cfg',
               '-d', args.start, '-f', args.end, '--level', 'LEVEL3A', '-w', outabs]

        process_command(cmd, wd)

        #for file in glob.glob(outabs+"/SENTINEL2*.zip"):
        #    print("unzipping"+file)
        #    with ZipFile(file, 'r') as zipObj:
        #        zipObj.extractall(outabs)
        #    os.remove(file)
    elif args.pipeline == 'scihub':
        wd = "/work/python/scihub_download/"
        #TODO: manage wd

        # wd = "/home/2019ca002/PycharmProjects/SoilMoistureService/scihub_download"
        outabs = os.path.abspath(args.outdir)
        if not os.path.exists(outabs):
            os.makedirs(outabs)

        cmd = ['python', 'Sentinel_download.py', '-a', 'apihub.txt',
               '-d', args.start.replace('-', ''), '-f', args.end.replace('-', ''),
               '--lat', str(s2_tile_centroid(args.tile)[1]), '--lon', str(s2_tile_centroid(args.tile)[0]),
               '-s', 'S2*L2A', '-w', outabs]

        process_command(cmd, wd)
    elif args.pipeline == 'peps':
        wd = "/work/python/peps_download"
        outabs = os.path.abspath(args.outdir)
        if not os.path.exists(outabs):
            os.makedirs(outabs)
        s2tiles_shp = gpd.read_file("/work/python/data/S2Tiles/S2Tiles.shp")
        t = s2tiles_shp[s2tiles_shp['Name'] == args.tile]
        print(t)
        cmd = ['python', 'peps_download.py', '-a', 'peps.txt', '-d', args.start, '-f', args.end, '-c', 'S1',
               '--lonmin', str(t.total_bounds[0].item()), '--lonmax', str(t.total_bounds[2].item()), '--latmin',
               str(t.total_bounds[1].item()), '--latmax', str(t.total_bounds[3].item()), '-p', 'GRD', "-m", "IW",
               '-r', 'Fast-24h', '-w', outabs]
        process_command(cmd, wd)
