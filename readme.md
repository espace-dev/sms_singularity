# Sentinel Soil Moisture at Plot scale (S2MP) Production Services

This module contains applications for Operational Soil Moisture Mapping over Agricultural Areas with Sentinel-1 and Sentinel-2

## User manual

* The [Soil Moisture Pipeline for agricultural areas user manual](https://gitlab.irstea.fr/loic.lozach/soilmoistureservices/-/blob/master/SoilMoistureServices_Manual.pdf) is available.

* The [Soil Moisture Pipeline Download Module user manual](https://gitlab.irstea.fr/loic.lozach/soilmoistureservices/-/blob/master/SoilMoistureServices_Manual_Download_Module.pdf) is available.

# Install

L’application containeurisée dans une image Singularity qu'il faut construire

```
sudo singularity build container_name SMS_definition
```
Une fois le "build" effectué on peut lancer le container en créant un lien entre l'hôte et le container
```
singularity run --writable-tmpfs -B /host/datapath:/data container_name
```
Une fois dans le container on peut utiliser les commandes classiques de la chaine. 
Il est aussi possible de lancer disrectement sms_pipeline

```
singularity exec --writable-tmpfs -B /host/datapath:/data container_name sms_pipeline.py -tile 31TEJ -start 2021-01-01 -end 2021-03-01 -outdir /data
```

# Usage

* Download Service
```
otbuser@d922b9b915cc:/data$ download_pipeline.py

```

* Calibration Service
```
otbuser@ce4dccfa13f8:/data$ calibration_service.py -h

usage: calibration_service.py [-h] -indir INDIR -inref INREF -zone ZONE
                              -outdir OUTDIR [-polar {vv,vh}]
                              [-theta {loc,elli}] [--overwrite]

S1 Calibration, orhorectification, dem, slope, aspect and incidence angle
processing

optional arguments:
  -h, --help         show this help message and exit
  -indir INDIR       Directory containing Sentinel1 .SAFE directories
  -inref INREF       Image reference, generally corresponding to a S2 tile
  -zone ZONE         Geographic zone reference for output files naming
  -outdir OUTDIR     Output directory
  -polar {vv,vh}     [Optional] Process VV or VH polarization, default vv
  -theta {loc,elli}  [Optional]Create local incidence raster or incidence
                     raster from ellipsoid, default loc
  --overwrite        [Optional] Overwrite already existing files (default
                     False)

```

* Preprocess Service
```
otbuser@ce4dccfa13f8:/data$ preprocess_service.py -h

usage: preprocess_service.py [-h]
                             {exmosS1,ndvi,gapfilling,stack,slope,gpm} ...

Preprocessing Services

positional arguments:
  {exmosS1,ndvi,gapfilling,stack,slope,gpm}
                        Preprocess pipeline
    exmosS1             Extract and mosaic Sentinel-1 to fit Sentinel-2 Tile
    ndvi                Compute cloud-masked NDVI from Sentinel2 or Landsat8.
    gapfilling          Perform Gapfilling over NDVI time serie, W
    stack               Create stacked and masked image from NDVI directory to
                        the input of a segmentation process
    slope               Extract SRTM1SecHGT with image ref and compute slope
    gpm                 Downloads, extracts and computes synthesis of NASA
                        Global Precipitation Model raster for TF model
                        selection (wet or dry)

optional arguments:
  -h, --help            show this help message and exit

```

* Production Service
```
otbuser@ce4dccfa13f8:/data$ production_service.py -h
usage: production_service.py [-h]
                             {short,mask,segment,segmentvect,serie,csvtomoist}
                             ...

Launch Soil Moisture pipeline with different level of processing.

positional arguments:
  {short,mask,segment,segmentvect,serie,csvtomoist}
                        Pipelines
    short               Inversion pipeline from user's masked labels, NDVI,
                        SarVV and SarIncidence
    mask                Mask labels image before inversion
    segment             Segment NDVI before masking and inversion
    segmentvect         Rasterize segmented NDVI vector before masking and
                        inversion
    serie               Batch inversion pipeline over a directory of
                        Sentinel-1 images
    csvtomoist          Apply CSV results on labels raster to create moisture
                        map

optional arguments:
  -h, --help            show this help message and exit

```

## License

Please see the license for legal issues on the use of the software (GNU Affero General Public License v3.0).

## Contact
Loïc Lozac'h (IRSTEA)
