import os

# # os.environ["PYTHONBREAKPOINT"]= "remote_pdb.set_trace"
# # os.environ["REMOTE_PDB_HOST"]= "127.0.0.1"
# # os.environ["REMOTE_PDB_PORT"]="4444"

import npyscreen
from configparser import ConfigParser
from pySmartDL import utils
import threading
import requests
from misc import Timer,Status
from payloadmanager import Payload
from payloadmanager import checksum_process_terminate
from npyscreenoverrides.widgets import CGrid,CMultiLineEdit

from collections import OrderedDict
import time
from requestmanager.requestmanager import SentinelQuerist
from requestmanager.requestmanager import ParsingError
from requestmanager.requestmanager import SentinelQueristError as sErr
from collections import deque 

# breakpoint()

__version_mjaor__ = 0
__version_minor__ = 3
__version_micro__ = 5
__version__ = "{}.{}.{}".format(__version_mjaor__, __version_minor__, __version_micro__)

# Main TUI
class MainTUI(npyscreen.NPSAppManaged):
    
    query_string=''
    esa_user =''
    esa_pass = ''
    outpath = ''
    checksum_download = None
    abandon_download = None
    exit_download = None
    
    # query check, query images, start download and exit_application
    menu_keys=['^S','^Q','^D','^E']

    
    def __init__(self,
                 esa_user='',
                 esa_pass='',
                 query_string=None,
                 checksum_download = None,
                 abandon_download = None,
                 exit_download = None
                 ):
        
         ##  load config file if exists
        if os.path.exists("/work/python/config.ini"):
            configs = read_config("/work/python/config.ini")
            valid_bool_T = ['true','yes','on','1']
            valid_bool_F = ['false','no','off','0']
            
            if 'username' in configs['ESA']:
                self.esa_user = configs['ESA']['username']
            
            if 'password' in configs['ESA']:
                self.esa_pass = configs['ESA']['password']
            
            if 'path' in configs['DOWNLOADS']:
                self.outpath = configs['DOWNLOADS']['path']
                
            if 'query_check' in configs['KEY_BINDINGS']:
                keys = configs['KEY_BINDINGS']['query_check'].split()
                if len(keys) != 4:
                    raise ValueError("required 4 keys, found: %d"%len(keys))
                
                for i,key in enumerate(keys):
                    if (len(key) != 2
                    or key[0] != '^'):
                        raise ValueError("malformed Key #%d; support for only ctrl+[KEY], received: %s"%(i,key))
                
                self.menu_keys = keys.copy()
            
            if not self.checksum_download:
                if 'checksum_download' in configs['APP']:
                    if configs['APP']['checksum_download'].lower() in valid_bool_T:
                        self.checksum_download = True
                    elif configs['APP']['checksum_download'].lower() in valid_bool_F:
                        self.checksum_download = False
                    else:
                        raise ValueError("checksum_download should be one of: [true|yes|on|1|false|no|off|0], received: %s"%configs['APP']['checksum_download'])
            
            if not self.abandon_download:
                if 'abandon_download' in configs['APP']:
                    if configs['APP']['abandon_download'].lower() in valid_bool_T:
                        self.abandon_download = True
                    elif configs['APP']['abandon_download'].lower() in valid_bool_F:
                        self.abandon_download = False
                    else:
                        raise ValueError("abandon_download should be one of: [true|yes|on|1|false|no|off|0], received: %s"%configs['APP']['abandon_download'])
            
            if not self.exit_download:
                if 'exit_download' in configs['APP']:
                    if configs['APP']['exit_download'].lower() in valid_bool_T:
                        self.exit_download = True
                    elif configs['APP']['exit_download'].lower() in valid_bool_F:
                        self.exit_download = False
                    else:
                        raise ValueError("exit_download should be one of: [true|yes|on|1|false|no|off|0], received: %s"%configs['APP']['exit_download'])    
        else:
            print("WARN: config.ini not found")
            print("workdir : "+os.getcwd())
            
        if esa_user:
            self.esa_user = esa_user
        
        if esa_pass:
            self.esa_pass = esa_pass
        
        if query_string:
            self.query_string = query_string
        
        if checksum_download:
            self.checksum_download = checksum_download
        
        if abandon_download:
            self.abandon_download = abandon_download
        
        if exit_download:
            self.exit_download = exit_download
        
        super(MainTUI,self).__init__()
    
    def onStart(self):
#        npyscreen.setTheme(npyscreen.Themes.BlackOnWhiteTheme)
        self.payloads = OrderedDict()
        self.addForm("MAIN", ConfigTUI, name="Configuration")
        self.addForm("DM", DMTUI,name="Download Manager")
          
        
# General Config TUI
class ConfigTUI(npyscreen.ActionFormWithMenus):
    
    esauser = ""
    esapass = ""
    outpath = ""
    query_string = ""
    row_to_key = dict()
    inline_readme = ""
    on_toggle_select_all = True
    concurrent_downloads = 1
    count_selected = 0
    sentinel_query = None
    # Perform Checksum on downloaded product
    checksum_download = False
    # Abandon retries after n downloads
    abandon_download = True
    
    def activate(self):
        self.edit()
#        self.parentApp.setNextForm("DM")
    
    def create(self):
        # Get the space used by the form
           
        if self.parentApp.esa_user:
            self.esauser = self.parentApp.esa_user
        
        if self.parentApp.esa_pass:
            self.esapass = self.parentApp.esa_pass
            
        if self.parentApp.outpath:
            self.outpath = self.parentApp.outpath
        
        if self.parentApp.query_string:
            self.query_string = self.parentApp.query_string
        
        if self.parentApp.checksum_download:
            self.checksum_download = self.parentApp.checksum_download
        
        if self.parentApp.abandon_download:
            self.abandon_download = self.parentApp.abandon_download
        
        y, x = self.useable_space()
        self.keypress_timeout = 1
        self.OK_BUTTON_TEXT = "Exit"
        self.CANCEL_BUTTON_TEXT = "Download"
        
        self.h_display_help
        
        package_directory = os.path.dirname(os.path.abspath(__file__))
        if os.path.exists(os.path.join(package_directory,"inline_readme.txt")):
            with open(os.path.join(package_directory,"inline_readme.txt"), 'r',encoding="utf-8") as readme:
                self.inline_readme = readme.read()
        
        ##########
        # MENUS  #
        ##########
        
        menu_keys = self.parentApp.menu_keys
        
        self.mainMenu = self.add_menu(name="Shortcuts", shortcut="^S")
        self.mainMenu.addItemsFromList([
            ("Check Query", self.h_query_check,menu_keys[0] ),
            ("Query Images", self.h_query,menu_keys[1] ),
            ("Start Download",   self.on_cancel, menu_keys[2]),
            ("Exit Application", self.on_ok, menu_keys[3]),
                ])
    
        ##########
        # BOXES  #
        ##########
        
        self.credentialsBox = self.add(npyscreen.BoxTitle,
                _contained_widget = npyscreen.TitleFixedText,
                relx = 1,
                rely = 1,
                editable = False,
                height = 5,
                width = 75,
                name = 'Esa Credentials')
        
        self.SelectionBox = self.add(npyscreen.BoxTitle,
                _contained_widget = npyscreen.TitleFixedText,
                relx = 1,
                rely = 6,
                editable = False,
                height = 4,
                width = 75,
                name = 'Images Download Location')
        
        self.QueryBox = self.add(npyscreen.BoxTitle,
                _contained_widget = npyscreen.TitleFixedText,
                relx = 1,
                rely = 10,
                editable = False,
                height = 15,
                width = 75,
                name = 'Query Editor')

        self.ReadmeBox = self.add(npyscreen.BoxTitle,
                _contained_widget = npyscreen.TitleFixedText,
                relx = 1,
                rely = 27,
                editable = False,
                height = 11,
                width = 75,
                name = '// Readme')
        
        self.GridBox = self.add(npyscreen.BoxTitle,
                _contained_widget = npyscreen.TitleFixedText,
                relx = 81,
                rely = 1,
                editable = False,
                width = 40,
                height = 34,
                name = 'Available Images',
                footer = "press 's'/'d' to select/deselect all")
               
        ##########
        #WIDGETS #
        ##########
        
        self.userName = self.add(npyscreen.TitleText,
                value = self.esauser,
                name = 'UserName: ',
                use_two_lines = False,
                rely = self.credentialsBox.rely + 2,
                relx = self.credentialsBox.relx + 1,
                width = 30,
                height = 1,
                begin_entry_at = 11,
                editable = True
                )
        
        self.password = self.add(npyscreen.TitlePassword,
                value = self.esapass,
                name = 'Password: ',
                use_two_lines = False,
                rely = self.credentialsBox.rely + 3,
                relx = self.credentialsBox.relx + 1,
                width = 30,
                height = 1,
                begin_entry_at = 11,
                editable = True
                )
                      
        self.outputPath = self.add(npyscreen.TitleFilenameCombo,
                value = self.outpath,
                name = 'Download Path: ',
                use_two_lines = False,
                rely = self.SelectionBox.rely + 2,
                relx = self.SelectionBox.relx + 1,
                width = 74,
                height=1,
                begin_entry_at = 14,
                editable = True,
                select_dir=True
                )
        
        self.QueryText = self.add(CMultiLineEdit,
                        height=13,
                        width= 73,
                        rely = self.QueryBox.rely + 1,
                        relx = self.QueryBox.relx + 1,
                        editable = True,
                        scroll_exit=True,
                        value=self.query_string)
        
        self.QueryCheckButton = self.add(npyscreen.ButtonPress,
                name = '<Check Query>',
                use_two_lines = False,
                rely = self.QueryBox.rely + self.QueryBox.height,
                relx = self.QueryBox.relx + 1,
                when_pressed_function=self.h_query_check
                )
        
        self.QueryButton = self.add(npyscreen.ButtonPress,
                name = '<Query Products>',
                use_two_lines = False,
                rely = self.QueryBox.rely + self.QueryBox.height,
                relx = self.QueryBox.relx + 22,
                when_pressed_function=self.h_query)
                
        
        self.InlineReadme = self.add(npyscreen.MultiLineEdit,
                name = '// Readme',
                value = self.inline_readme,
                relx = self.ReadmeBox.relx + 1,
                rely = self.ReadmeBox.rely + 1,
                editable=False,
                width = 70,
                )
        
        self.ImagesGird = self.add(CGrid,
                columns = 3,
                col_titles = ['( )','Identifier','Status'],
                select_whole_line = True,
                rely = self.GridBox.rely + 1,
                relx = self.GridBox.relx + 1,
                editable = False,
                col_widths = [5,20,8],
                width = 33,
                height = 31,
                slow_scroll = True,
                name = "imgGrid"
                )
        
        self.GridMoreLabel = self.add(npyscreen.FixedText,
                                      value = '- more -',
                                      relx = self.GridBox.relx+1,
                                      rely = self.GridBox.rely + self.GridBox.height - 2,
                                      color='CONTROL',
                                      editable = False,
                                      hidden = True)
        
        self.ImagesGird.add_handlers({"x": self.h_select,
                                      "s": self.h_toggle_all_select,
                                      "d": self.h_toggle_all_deselect
                                      })
        
    def while_waiting(self):
          
        grid = self.ImagesGird
        if(grid.values and ((grid.height + grid.begin_row_display_at - 2) < (len(grid.values)))):
            self.GridMoreLabel.hidden = False
        else:
            self.GridMoreLabel.hidden = True
        self.display()
    
    # exit
    def on_ok(self):
        notify_result = npyscreen.notify_ok_cancel("Are you sure you want to exit ?", title='Exit ?',form_color='CONTROL',wrap=True,editw = 1)
        if(notify_result):
            self.parentApp.setNextForm(None)
            
    # proceed to download
    def on_cancel(self):
        
        if not os.path.isdir(self.outputPath.value):
            warning_message = "Path: {}\nIs not a directory. Please choose a valid directory path".format(self.outputPath.value)
            npyscreen.notify_confirm(warning_message, title='Invalid Path',form_color='DANGER',wrap=True,editw = 1)
        
        elif not os.path.exists(self.outputPath.value):
            warning_message = "Path: {}\nDoes not exist. Create Directory ?".format(self.outputPath.value)
            notify_result = npyscreen.notify_yes_no(warning_message, title='Directory not Found',form_color='CAUTION',wrap=True,editw = 1)
            if(notify_result):
                try:
                    os.mkdir(self.outputPath.value)
                    npyscreen.notify_confirm("Directory Created", title='Directory Created',form_color='CAUTION',wrap=True,editw = 1)
                except Exception as e:
                    npyscreen.notify_confirm("Could not Create Directory\nError:%s"%e, title='Directory Creatation Failed',form_color='Critical',wrap=True,editw = 1)

        elif len(self.parentApp.payloads.keys()) > 0:
            for payload in self.parentApp.payloads.values():
                payload[0].output_dir = self.outputPath.value
            DMForm = self.parentApp.getForm("DM")
            DMForm.concurrent_downloads = self.concurrent_downloads
            DMForm.count_selected = self.count_selected
            DMForm.PopulateGrid()
            self.parentApp.switchForm("DM")
            
        elif len(self.parentApp.payloads.keys()) == 0:
            npyscreen.notify_confirm("Nothing to Download", title='Empty List',form_color='CAUTION',wrap=True,editw = 1)
            
            
    def h_select(self, inpt):
        if(self.ImagesGird.edit_cell != -1):
            
            row = self.ImagesGird.edit_cell[0]
            (pl,value) = self.parentApp.payloads[self.row_to_key[row]]
            pl.toggle_download()
            if(self.ImagesGird.values[row][0] == '(X)'):
                 self.ImagesGird.values[row][0] = '( )'
                 self.count_selected -=1
            else:
                self.ImagesGird.values[row][0] = '(X)'
                self.count_selected +=1
    
    def h_toggle_all_deselect(self,inpt):
        self.a_toggle_all_select(False)
        
    def h_toggle_all_select(self,inpt):
        self.a_toggle_all_select(True)
        
    def a_toggle_all_select(self,select=True):
        
        select_text = "(X)"
        select_img = True
        add_to_selected = 1
        
        if not select:
            select_text = "( )"
            select_img = False
            self.on_toggle_select_all = True
            add_to_selected = -1
        else:
            self.on_toggle_select_all = False
            add_to_selected = 1
            
        for row in range(0,len(self.ImagesGird.values)):
            pl,value = self.parentApp.payloads[self.row_to_key[row]]
            pl.queued_for_fownload = select_img
            self.ImagesGird.values[row][0] = select_text
            self.count_selected += add_to_selected
                              
    def h_query_check(self):
        npyscreen.notify("Parsing Query...", title='Searching...')
        
        try:
            self.sentinel_query = SentinelQuerist(self.userName.value, self.password.value,)
            prods_count = self.sentinel_query.query_check(self._strip_empty_lines(self.QueryText.value))
            npyscreen.notify_confirm("%d products match your query"%prods_count,title="Searching...")
        
        except ParsingError as pe:
            npyscreen.notify_confirm(str(pe),'Parsing Error',form_color='DANGER',wrap=True,editw=1)
        # Sentinel Querist error
        except sErr as e:
            npyscreen.notify_confirm(str(e), title='Server Error...',form_color='DANGER',wrap=True,editw = 1)
        # requests raised errors
        except requests.exceptions.HTTPError as e:
            npyscreen.notify_confirm(str(e), title='HTTP Error',form_color='DANGER',wrap=True,editw = 1)
        except requests.exceptions.ConnectionError as e:
            npyscreen.notify_confirm(str(e), title='Connection Error',form_color='DANGER',wrap=True,editw = 1)
        except requests.exceptions.Timeout as e:
            npyscreen.notify_confirm(str(e.response), title='Timeout Error [{}]'.format(1),form_color='DANGER',wrap=True,editw = 1)
        # Other errors
        except Exception as e:
            npyscreen.notify_confirm(str(e), title='Error...',form_color='DANGER',wrap=True,editw = 1)
            
    def h_query(self):
        npyscreen.notify("Preparing Query...", title='Searching...')
        products = {}
        try:
            self.sentinel_query = SentinelQuerist(self.userName.value, self.password.value,)
            prods1 = self.sentinel_query.query_yield(self._strip_empty_lines(self.QueryText.value))
            cumul_result_count =0

            for query_result,result_len,count in prods1:
                cumul_result_count+=result_len
                products.update(query_result)
                npyscreen.notify("Loaded: %d of %d"%(cumul_result_count,count), title='Searching...')
            
            if len(products) > 0:
                self.ImagesGird.values = []
                Payload.ESA_CREDENTIALS = self.userName.value,self.password.value
                
                payloads = self.parentApp.payloads
                payloads.clear()
                self.count_selected = 0
                self.row_to_key.clear()
                product_count = len(products)
                
                for rowi,key in enumerate(products):
                    prodName = products[key]['identifier']
                    prodName = prodName[0:4]+prodName[17:32]
                    status = Status.Offline
                    row = []
                    product_info = self.sentinel_query.get_product_odata(key)
                    row.append('(X)')
                    self.count_selected +=1
                    row.append(prodName)
                    if(product_info['Online']):
                        row.append('Online')
                        status = Status.Online
                    else:
                        row.append('Offline')
                        
                    self.ImagesGird.values.append(row)
                    payloads[key] = [Payload(key,
                                             products[key]['link'],
                                             self.outputPath.value,products[key]['identifier'],
                                             product_info['md5'],
                                             product_info['size'],
                                             status=status,
                                             checksum_download = self.checksum_download,
                                             abandon_download = self.abandon_download
                                             ),
                                     -1]
                    self.row_to_key[rowi] = key
                    npyscreen.notify("Fetching additional data:\nFetched for %d of %d products"%(rowi,product_count), title='Fetching...')
                
                if rowi > 0:
                    self.ImagesGird.editable = True
                    self.ImagesGird.begin_row_display_at = 0
                    self.ImagesGird.begin_col_display_at = 0
                    self.ImagesGird.edit_cell = [0, 0]
                    self.h_selectGridWidget()
                self.display()
                
        except ParsingError as pe:
            npyscreen.notify_confirm(str(pe),'Parsing Error',form_color='DANGER',wrap=True,editw=1)
        # Sentinel Querist error
        except sErr as e:
            npyscreen.notify_confirm(str(e), title='Server Error...',form_color='DANGER',wrap=True,editw = 1)
        # requests raised errors
        except requests.exceptions.HTTPError as e:
            npyscreen.notify_confirm(str(e), title='HTTP Error',form_color='DANGER',wrap=True,editw = 1)
        except requests.exceptions.ConnectionError as e:
            npyscreen.notify_confirm(str(e), title='Connection Error',form_color='DANGER',wrap=True,editw = 1)
        except requests.exceptions.Timeout as e:
            npyscreen.notify_confirm(str(e.response), title='Timeout Error [{}]'.format(1),form_color='DANGER',wrap=True,editw = 1)
        # Other errors
        except Exception as e:
            npyscreen.notify_confirm(str(e), title='Error...',form_color='DANGER',wrap=True,editw = 1)
            
    def _strip_empty_lines(self,text):
        string_ = "\n".join([ll.rstrip() for ll in text.splitlines() if ll.strip()])

        return string_
            
    def h_selectGridWidget(self):
        r = list(range(0, len(self._widgets__)))
        for n in r:
            if self._widgets__[n].name == "imgGrid":
                
                if self.editw > n :
                    self._widgets__[self.editw].h_exit_up(True)
                else:
                    self._widgets__[self.editw].h_exit_up(True)
                    
                self._widgets__[self.editw].editing = False
                try:
                    self._widgets__[self.editw].entry_widget.editing = False
                except:
                    pass
    
                self.editw = n
                self._widgets__[self.editw].edit()
                self.display()
                

        
class DMTUI(npyscreen.ActionFormMinimal):

    rowToKey = dict()
    checker_requester_thread = None
    thread_stop = False
    main_timer = Timer()
    concurrent_downloads = 1
    count_selected = 0
    exit_download=True

    Q_offline = deque()
    Q_req = deque()
    Q_online = deque()
    Q_dlded = deque()
    Q_hashed = deque() 
    
    offline_last_time = 0
    req_last_time = 0
    OFFLINE_RECHECK_PERIOD = 120
    REQUEST_RECHECK_PERIOD = 90
    
    def activate(self):
        self.edit()
        self.parentApp.setNextForm(None)
        self.keypress_timeout = 1
        
    def create(self):
        
        if self.parentApp.exit_download:
            self.exit_download = self.parentApp.exit_download
        
        # Get the space used by the form
        y, x = self.useable_space()
        self.keypress_timeout = 1
        self.OK_BUTTON_TEXT = "Exit"

        self.StatBox = self.add(npyscreen.BoxTitle,
                _contained_widget = npyscreen.TitleFixedText,
                relx = 1,
                rely = 1,
                editable = False,
                width = 104,
                height = 4,
                name = 'Internal Stats')
        
        self.ProductProgressInfo = self.add(npyscreen.TitleText,
                value = '-',
                name = 'Products Progress: ',
                use_two_lines = False,
                rely = self.StatBox.rely + 1,
                relx = self.StatBox.relx + 2,
                height = 1,
                begin_entry_at = 18,
                editable = False,
                width = 75
                )
        
        self.RecheckCounterInfo = self.add(npyscreen.TitleText,
                value = '-',
                name = 'Rechecks in: ',
                use_two_lines = False,
                rely = self.StatBox.rely + 2,
                relx = self.StatBox.relx + 2,
                height = 1,
                begin_entry_at = 14,
                editable = False,
                width = 75
                )
        
        
        self.GridBox = self.add(npyscreen.BoxTitle,
                _contained_widget = npyscreen.TitleFixedText,
                relx = 1,
                rely = 5,
                editable = False,
                width = 104,
                height = 18,
                name = 'Download Status')
        
        self.ConsoleBox = self.add(npyscreen.BoxTitle,
                _contained_widget = npyscreen.TitleFixedText,
                relx = 1,
                rely = 24,
                editable = False,
                width = 104,
                height = 15,
                name = 'Console')
        
        self.Console = self.add(npyscreen.Pager,
                                relx = self.ConsoleBox.relx+1,
                                rely = self.ConsoleBox.rely+1,
                                width = 102,
                                height = 14,
                                max_height=13,
                                slow_scroll=True,
                                autowrap=True)
        
        self.Console.add_handlers({"^P": self.h_console_previous,
                                   "^N": self.h_console_next
                                   })
        
        self.ImagesGird = self.add(CGrid,
                columns = 5,
                col_titles = ['Identifier','Status','Progress','Rate','ETA'],
                select_whole_line = True,
                rely = self.GridBox.rely + 1,
                relx = self.GridBox.relx + 1,
                editable = False,
                col_widths = [20,13,42,10,15,],
                width = 100,
                height = 15,
                slow_scroll = True,
                )
        
        self.GridMoreLabel = self.add(npyscreen.FixedText,
                                      value = '- more -',
                                      relx = self.GridBox.relx+1,
                                      rely = self.GridBox.rely + self.GridBox.height - 2,
                                      color='CONTROL',
                                      editable = False,
                                      hidden = True)
        self.add_handlers({"^S":self.h_check_checker
                          })
    
    def h_check_checker(self,inpt):
        for t in threading.enumerate():
            if t.name == "Checker Thread":
                if t.is_alive():
                    npyscreen.notify_confirm("Checker Thread is still running")
                    return
                else:
                    npyscreen.notify_confirm("Checker Thread is dead")
                    return
        npyscreen.notify_confirm("Couldn't find thread")
        
    def while_waiting(self):
        
        if self.ImagesGird.edit_cell != -1 :
            row = self.ImagesGird.edit_cell[0]
            log = self.parentApp.payloads[self.rowToKey[row]][0].log_text
            self.Console.clear()
            self.Console.values = log.getvalue().splitlines(True)
            self.Console.display()
        
        offline_recheck_progress = 1 - min(1,max(0,(time.time()-self.offline_last_time)/self.OFFLINE_RECHECK_PERIOD))
        req_recheck_progress = 1 - min(1,max(0,(time.time()-self.req_last_time)/self.REQUEST_RECHECK_PERIOD))
        
        recheck_counters_progress = ''
        if self.Q_offline:
            recheck_counters_progress+= utils.progress_bar(offline_recheck_progress,length=10)
        
        if self.Q_req:
            if self.Q_offline:
                recheck_counters_progress+= ' || '
            recheck_counters_progress+= utils.progress_bar(req_recheck_progress,length=10)
        
        if not recheck_counters_progress:
            recheck_counters_progress = '-No offline or requested products to process-'
        
        self.RecheckCounterInfo.value = recheck_counters_progress
        
        # show or hide the - more - button in the grid 
        grid = self.ImagesGird
        if(grid.values and ((grid.height + grid.begin_row_display_at - 2) < (len(grid.values)))):
            self.GridMoreLabel.hidden = False
            
        # Downloads Manager
        if self.Q_online:
            payload,iRow = self.Q_online[0]
            if payload.status == Status.Online:
                payload.configure_downloader()
                payload.download()
            elif payload.status_change:
                if payload.status == Status.Downloading:
                    self.ImagesGird.values[iRow][1] = str(payload.status)
                    if payload.downloader:
                        self.ImagesGird.values[iRow][2] = payload.downloader.get_progress_bar(length=15) + " ({} of {})".format(payload.downloader.get_dl_size(human=True),utils.sizeof_human(payload.size))
                        self.ImagesGird.values[iRow][3] = payload.downloader.get_speed(human=True)
                        self.ImagesGird.values[iRow][4] = utils.time_human(payload.downloader.get_eta(),fmt_short=True)
                    payload.auto_update_status()
                if payload.status == Status.Succeeded:
                    self.ImagesGird.values[iRow][1] = str(payload.status)
                    self.ImagesGird.values[iRow][2] = 'file downloaded successfully'
                    self.ImagesGird.values[iRow][3] = " "
                    self.ImagesGird.values[iRow][4] = " "
                    self.Q_hashed.append(self.Q_online.popleft())
                if payload.status == Status.Hash:
                    self.ImagesGird.values[iRow][1] = str(payload.status)
                    self.ImagesGird.values[iRow][2] = "Downloaded {} of {} in {}".format(utils.sizeof_human(payload.downloader.get_dl_size()),utils.sizeof_human(payload.downloader.filesize),utils.time_human(payload.downloader.get_dl_time(),fmt_short=True))
                    self.ImagesGird.values[iRow][3] = " "
                    self.ImagesGird.values[iRow][4] = " "
                    payload.status = Status.Hash
                    self.Q_dlded.append(self.Q_online.popleft())
                if payload.status == Status.Failed:
                    self.ImagesGird.values[iRow][1] = str(payload.status)
                    self.ImagesGird.values[iRow][2] = "Download Failed, requeuing"
                    self.ImagesGird.values[iRow][3] = " "
                    self.ImagesGird.values[iRow][4] = " "
                    payload.status = Status.Online
                    self.Q_online.append(self.Q_online.popleft())
                if payload.status == Status.Offline:
                    self.ImagesGird.values[iRow][1] = str(payload.status)
                    self.ImagesGird.values[iRow][2] = "Download Failed, set as offline"
                    self.ImagesGird.values[iRow][3] = " "
                    self.ImagesGird.values[iRow][4] = " "
                    self.Q_offline.append(self.Q_online.popleft())
                
                self.ProductProgressInfo.value = self.get_product_progress_info()
                    
        if self.Q_dlded:
            payload,iRow = self.Q_dlded[0]
            if payload.status_change:
                if payload.status == Status.Succeeded:
                    self.ImagesGird.values[iRow][1] = str(payload.status)
                    self.ImagesGird.values[iRow][2] = 'file downloaded successfully'
                    self.ImagesGird.values[iRow][3] = " "
                    self.ImagesGird.values[iRow][4] = " "
                else:
                    self.ImagesGird.values[iRow][1] = str(payload.status)
                    self.ImagesGird.values[iRow][2] = 'file downloaded but seems corrupted'
                    self.ImagesGird.values[iRow][3] = " "
                    self.ImagesGird.values[iRow][4] = " "
                self.Q_hashed.append(self.Q_dlded.popleft())
                self.ProductProgressInfo.value = self.get_product_progress_info()
            else:
                payload.auto_update_status()
                self.Q_dlded.append(self.Q_dlded.popleft())
                self.ProductProgressInfo.value = self.get_product_progress_info()
        
        if (self.exit_download 
            and not self.Q_offline
            and not self.Q_req
            and not self.Q_online
            and not self.Q_dlded
            ):
            # call exit
            self.on_ok(True)
       
        self.display()
        
    def on_ok(self,silent=False):
        notify_result = silent
        if not silent:
            notify_result = npyscreen.notify_ok_cancel("Are you sure you want to exit ?", title='Exit ?',form_color='CONTROL',wrap=True,editw = 1)
        if(notify_result):
            npyscreen.notify("Cleaning up before Exiting", title='Exiting')
            checksum_process_terminate()
            for payload,iRow in self.parentApp.payloads.items():
                try:
                    payload.downloader.stop()
                except Exception:
                    pass
            self.thread_stop = True
            while self.checker_requester_thread.is_alive():
                pass
        self.parentApp.switchForm(None)
            
    def PopulateGrid(self):
        currentGridRow = 0
        self.ImagesGird.values = []
        payloads = self.parentApp.payloads

        for already_processed,(key,(payload,irow)) in enumerate(payloads.items()):
            row = []
            if(payload.queued_for_download):
                npyscreen.notify("Fetching additional data:\nFetched for {} of {} products".format(already_processed,self.count_selected), title='Fetching Data',wrap=True)
                payloads[key][1] = currentGridRow
                # payloads[key][0].configure_downloader()
                self.rowToKey[currentGridRow] = key
                row.append(payload.product_short_name)
                if(payload.status == Status.Online):
                    row.append('Online')
                    self.Q_online.append(payloads[key])
                else:
                    row.append('Offline')
                    self.Q_offline.append(payloads[key])
                row.append(' (0 MB of {})'.format(utils.sizeof_human(payload.size)))
                row.append(' ')
                row.append(' ')
                currentGridRow += 1
                self.ImagesGird.values.append(row)
        
        
        self.offline_last_time = time.time()
        self.req_last_time = time.time()

        
        self.checker_requester_thread  = threading.Thread(name="Request Checker",target=self.checker_requester_fun, args=())
        self.checker_requester_thread.isDaemon = True
        self.checker_requester_thread.name = "Checker Thread"
        self.checker_requester_thread.start()
        self.ImagesGird.editable = True
        self.ProductProgressInfo.value = self.get_product_progress_info()        
    
    def checker_requester_fun(self):

        while True:
            
            if self.thread_stop == True:
                break

            if self.Q_offline:
                payload,iRow = self.Q_offline[0]
                if payload.status  == Status.Requested:
                    self.ImagesGird.values[iRow][1] = str(payload.status)
                    self.Q_req.append(self.Q_offline.popleft())
                elif payload.status == Status.Online:
                    self.ImagesGird.values[iRow][1] = str(payload.status)
                    self.Q_online.append(self.Q_offline.popleft())
                    self.ProductProgressInfo.value = self.get_product_progress_info()
                else:
                    self.offline_last_time = _invoke_timed_callback(self.offline_last_time, payload.auto_update_status, self.OFFLINE_RECHECK_PERIOD)
            
            if self.Q_req:
                payload,iRow = self.Q_req[0]
                if payload.status == Status.Online:
                    self.ImagesGird.values[iRow][1] = str(payload.status)
                    self.Q_online.append(self.Q_req.popleft())
                    self.ProductProgressInfo.value = self.get_product_progress_info()
                else:
                    self.Q_req.append(self.Q_req.popleft())
                    self.ProductProgressInfo.value = self.get_product_progress_info()
                    self.req_last_time = _invoke_timed_callback(self.req_last_time, payload.auto_update_status,self.REQUEST_RECHECK_PERIOD)
                        
            time.sleep(10)
                        
    #define ^p and ^n key presses when in console for consistency with the previous TUI
    def h_console_previous(self,inpt):
        self.Console.h_exit_up(inpt)
    
    def h_console_next(self,inpt):
        self.Console.h_exit_down(inpt)
    
    def get_product_progress_info(self):
        info_string = "OF:{0:>4} || R:{1:>4} || ON:{2:>4} || D:{3:>4} || H:{4:>4}"
        info_string = info_string.format(len(self.Q_offline),len(self.Q_req),len(self.Q_online),len(self.Q_dlded),len(self.Q_hashed))
        return info_string

def read_config(path:str=None):
    config = ConfigParser()
    config.read(path)

    sections_dict = {}

    # get all defaults
    defaults = config.defaults()
    temp_dict = {}
    for key in defaults.keys():
        temp_dict[key] = defaults[key]

    sections_dict['default'] = temp_dict

    # get sections and iterate over each
    sections = config.sections()
    
    for section in sections:
        options = config.options(section)
        temp_dict = {}
        for option in options:
            temp_dict[option] = config.get(section,option)
        
        sections_dict[section] = temp_dict

    return sections_dict

def _invoke_timed_callback(
        reference_time, callback_lambda, callback_period):
    """Invoke callback if a certain amount of time has passed.
    This is a convenience function to standardize update callbacks from the
    module.
    Parameters:
        reference_time (float): time to base ``callback_period`` length from.
        callback_lambda (lambda): function to invoke if difference between
            current time and ``reference_time`` has exceeded
            ``callback_period``.
        callback_period (float): time in seconds to pass until
            ``callback_lambda`` is invoked.
    Returns:
        ``reference_time`` if ``callback_lambda`` not invoked, otherwise the
        time when ``callback_lambda`` was invoked.
    """
    current_time = time.time()
    if current_time - reference_time > callback_period:
        callback_lambda()
        return current_time
    return reference_time