import time
import enum
from logging import getLogger
import logging

class Timer():
    def __init__(self):
        self._max=125
        self._tick=0
        self._running = False
    def tick(self):
        """ Call this to start timer """
        # Store the current time in _tick
        self._tick=time.time()
        self._running = True
        
    def tock(self):
        """ Call this to stop timer """
        # Add the delta in the times-list
#        if time.time()-self._tick > self._max :
#            self.tick()
        return (time.time()-self._tick)
        
    def half_tick(self):
        if not self._running:
            self.tick()
        self._tick = self._max/2
    
    def running(self):
        return self._running
    
    def stop(self):
        self._running = False
        
    def has_elapsed(self,seconds):
        if time.time() - self._tick >= seconds:
            return True
        else:
            return False


class Status(enum.Enum):
    
    Offline=0
    Pending = 1
    Requested=2
    Online=3
    Downloading=4
    Downloaded=5
    Hash=6
    Succeeded=7
    Failed = 8
    Corrupted=9
     
    # TODO: replace enums with aenums later
    def __str__(self):
        
        if self.value == 0:
            return "Offline"
        elif self.value == 1:
            return "Pending"
        elif self.value == 2:
            return "Requested"
        elif self.value == 3:
            return "Online"
        elif self.value == 4:
            return "Downloading"
        elif self.value == 5:
            return "Downloaded"
        elif self.value == 6:
            return "Hashing"
        elif self.value == 7:
            return "Succeeded"
        elif self.value == 8:
            return "Failed"
        elif self.value == 9:
            return "Corrupted"

        
    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self.value < other.value
        return False
    
    def __le__(self, other):
        if isinstance(other, self.__class__):
            return self.value <= other.value
        return False
    
    def __gt__(self, other):
        if isinstance(other, self.__class__):
            return self.value > other.value
        return False
    
    def __ge__(self, other):
        if isinstance(other, self.__class__):
            return self.value >= other.value
        return False
    
    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.value != other.value
        return False
    
    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.value == other.value
        return False
    
def setup_logger(key,sio):
        
        fmt = '%(asctime)s [%(levelname)s] %(message)s'
        datefmt='%d/%d %H:%M'
        
        log = getLogger(key)
        formatter = logging.Formatter(fmt,datefmt)
        console = logging.StreamHandler(sio)
        console.setFormatter(formatter)
        log.addHandler(console)
        log.setLevel("INFO")
        return log
