#!/usr/bin/env python

'''
File:          shp_convex_hull.py
Description:   This code generates convex hull shapefile for point, line and polygon shapefile
'''
import os

try:
    import ogr
except ImportError:
    from osgeo import ogr

def get_convex_hull(shp_path,in_mem='a',in_mem_less_mb=200):

    #three point, line and polygon example shapefile
    _shp = shp_path
    base=os.path.basename(_shp)
    _shp_filename = os.path.splitext(base)[0]
    # will the convex shapefile be created in memory ?
    is_in_mem = False
    
    #open the shapefile
    datasource = ogr.Open(_shp)
    
    #output shapefile name (convex hull)
    convex_hull_shp = '{0}_convex_hull.shp'.format(_shp_filename)
    #output shapefile layer name
    layer_name = 'convex_hull_layer'
    
    #by default set driver to shapefile to be able to create a convex hull shapefile
    driver = ogr.GetDriverByName('ESRI Shapefile')
    
    #if the user wants, a shapefile can be created in memory.
    if in_mem == 'y' :
        #create convex hull shapefile datasource in memory
        driver=ogr.GetDriverByName('MEMORY')
        is_in_mem = True
    
    #if in_mem set to a (auto), a shapefile is created in memory
    elif in_mem == 'a':
        shp_size = os.path.getsize(shp_path)
        if shp_size >> 20 < in_mem_less_mb:
            driver=ogr.GetDriverByName('MEMORY')    
            is_in_mem = True
        
    convex_hull_datasource = driver.CreateDataSource(convex_hull_shp)
    
    #get number of layers of input shapefile
    layer_count = datasource.GetLayerCount()
    
    for layer in range(layer_count):
        #get layer of input shapefile
        shp_layer = datasource.GetLayerByIndex(layer)
        #get spatial reference of input shapefile
        srs = shp_layer.GetSpatialRef()
    
        #create convex hull layer
        convex_hull_layer = convex_hull_datasource.CreateLayer(layer_name, srs, ogr.wkbPolygon)
    
        #get number of features of input shapefile
        shp_feature_count = shp_layer.GetFeatureCount()
    
        #define convex hull feature
        convex_hull_feature = ogr.Feature(convex_hull_layer.GetLayerDefn())
    
        #define multipoint geometry to store all points
        multipoint = ogr.Geometry(ogr.wkbMultiPoint)
    
        for each_feature in range(shp_feature_count):
            #get feature from input shapefile
            shp_feature = shp_layer.GetFeature(each_feature)
            #get input feature geometry
            feature_geom = shp_feature.GetGeometryRef()
            #if geometry is MULTIPOLYGON then need to get
            #   POLYGON then LINEARRING to be able to get points
            if feature_geom.GetGeometryName() == 'MULTIPOLYGON':
                for polygon in feature_geom:
                    for linearring in polygon:
                        points = linearring.GetPoints()
                        for point in points:
                            point_geom = ogr.Geometry(ogr.wkbPoint)
                            point_geom.AddPoint(point[0], point[1])
                            multipoint.AddGeometry(point_geom)
            #if geometry is POLYGON then need to get
            #   LINEARRING to be able to get points
            elif feature_geom.GetGeometryName() == 'POLYGON':
                for linearring in feature_geom:
                    points = linearring.GetPoints()
                    for point in points:
                        point_geom = ogr.Geometry(ogr.wkbPoint)
                        point_geom.AddPoint(point[0], point[1])
                        multipoint.AddGeometry(point_geom)
            #if geometry is MULTILINESTRING then need to get
            #   LINESTRING to be able to get points
            elif feature_geom.GetGeometryName() == 'MULTILINESTRING':
                for multilinestring in feature_geom:
                    for linestring in multilinestring:
                        points = linestring.GetPoints()
                        for point in points:
                            point_geom = ogr.Geometry(ogr.wkbPoint)
                            point_geom.AddPoint(point[0], point[1])
                            multipoint.AddGeometry(point_geom)
            #if geometry is MULTIPOINT then need to get
            #   POINT to be able to get points
            elif feature_geom.GetGeometryName() == 'MULTIPOINT':
                for multipoint in feature_geom:
                    for each_point in multipoint:
                        points = each_point.GetPoints()
                        for point in points:
                            point_geom = ogr.Geometry(ogr.wkbPoint)
                            point_geom.AddPoint(point[0], point[1])
                            multipoint.AddGeometry(point_geom)
            #if the geomerty is POINT or LINESTRING then get points
            else:
                points = feature_geom.GetPoints()
            for point in points:
                point_geom = ogr.Geometry(ogr.wkbPoint)
                point_geom.AddPoint(point[0], point[1])
                multipoint.AddGeometry(point_geom)
        #convert multipoint to convex hull geometry
        convex_hull = multipoint.ConvexHull()
        #set the geomerty of convex hull shapefile feature
        convex_hull_feature.SetGeometry(convex_hull)
        #add the feature to convex hull layer
        convex_hull_layer.CreateFeature(convex_hull_feature)
    
    if is_in_mem:
        return convex_hull_datasource
    else:
        return None
    
def shp_to_wkt(shp):
    datasource = None
    if isinstance(shp, str):
        datasource = ogr.Open(shp)
    elif isinstance(shp,ogr.DataSource):
        datasource = shp
    else:
        raise TypeError("Requires shape file path or os.geo.Datasource received %s"%type(shp))
    
    #get first layer
    shp_layer = datasource.GetLayerByIndex(0)
    
    #get first feature
    shp_feature = shp_layer.GetFeature(0)
    feature_geom = shp_feature.GetGeometryRef()
    _wkt = '{gtype}(({coors}))'
    _coor_tuple = '%.4f %.4f'
    _coors = ''
    _first = True
    
    geom_type = feature_geom.GetGeometryName()
    
    for linearring in feature_geom:
        points = linearring.GetPoints()
        for point in points:
            if not _first:
                _coors += ','
            else:
                _first = False
            _coors += _coor_tuple%(point[0],point[1])
    
    
    wkt = _wkt.format(gtype=geom_type,coors=_coors)
    # return feature_geom.ExportToWkt()
    return wkt

def geojson_to_wkt(geojson):
    datasource = ogr.Open(geojson)
     #get first layer
    gjson_layer = datasource.GetLayerByIndex(0)
    
    #get first feature
    gjson_feature = gjson_layer.GetFeature(0)
    print(gjson_feature.GetGeometryRef().ExportToWkt())
    return gjson_feature.GetGeometryRef().ExportToWkt()